---
title: Algal growth control and monitor
subtitle:
comments: false
---
### This is my main project page for fabacademy fablab2022 & fablab2023, scroll down for the 2022-content

## Project status at the end of fabacademy (2023)

The project has gone through a transformation during the last year of development.
After discussing with biologists, it became clear that the growth system needed to facilitate much more experimental points (e.g. growth tanks).

Eventually the growth system was simplified to be a simple pH-dependet CO2-dosing unit with temperature sensors and data collection.
The lamps, peristaltic pumps etc. were all removed. These features would have been nice to have, but only for a larger-scale system,
for a number of ~20 1L cultivation tanks, automation of all features is not simply worth it timewise.

The project is shown in the pictures below, with the different sections highlighted.
- **TBD add pictures**



The current iteration is divided into several subsystems as outlined in the [Bill of materials](https://karlpalo.gitlab.io/fablab2022/mainproject/bom/)
These subsystems are:

- **I2C-Electronics** Rasperry Pi 3+ (main processing, interface and data collection unit)
- **Feather M0-sensorboard** Feather M0 sensorboards x 3 (smaller peripheral boards to operate the sensors and valves, controlled by Rasperry Pi)
- **Electronics** A socketed breadboard with a feather M0 as a core, with the functionality needed to support the sensors and peripheral devices
- **Pneumatics** Power supply, cables, sensors, and solenoid gas valves

## The Algae growth system (2022)

The goal is to build a system for growing algae for scientific experiments.

The first challenge is to build an aqueous growth environment, in essence a "fish tank" for algae. Projects like this have been done in the history of fabacademy in the context of aquaponics. (E.g, projects like *[Aquapioneers](http://aquapioneers.io/plants/)* & *[Assistive Aquaponics Fish Tank](http://fabacademy.org/2021/labs/charlotte/students/theodore-warner/Final%20Project/final-project/)* )

In addition to the challenge of building an aqueous cultivation system, the system needs also to be highly adjustable (to allow setting and modifying of experimental variables) and preferably have a great degree of modularity and scalability (to facilitate experimental matrixes of different sizes). **Furthermore, as the system aims for production of scientific data, the validity and accuracy of data and accurate control of system variables is absolutely vital!**

The system will be designed in several modular parts:
- The Growth tank (grey in image)
- The Sensory box (yellow in image)
- The CO2-feed system (blue in image)
- The Nutrient feed system (green in image)
- The Control box + display (red in image)
- The water flow control pump/The water level control (multiple ways to do this!)


[![Picture of growth system](../mainproject-img/ver1_system-thumb.png)](../mainproject-img/ver1_system.png)


The initial experiment the system will be used in will be the subject of a scientific paper.

Here will be links to the project milestones and a description of the project in general



## Project status at the end of fabacademy fablab2022

Unfortunately the project is not finished yet. It needs several functionalities to be suitable for the intended purpose:
- Only one growth module is "ready"
- The camera is not implemented yet
- Data logging is not implemented
- Packaging and system integration needs to be done

What is done however:
- User inteface via flask
- Python I2C-communication with the peripheral devices
- the peripheral devices themselves


# COOL VIDEO (2022)
[![Picture of growth system](/videosplash.png)](/fablab2022_H.264.mp4)


# Poster (2022)
[![Picture of poster](/poster.jpg)](/poster.jpg)


**The work will continue!!!**
