---
title: Electronics
subtitle:
comments: false
---
###  The Algae growth system

## Electronical Design

During the development of the project, it was deemed most resonable to run the project with a Rasperry Pi as its heart. This means most of the external electronics would be as minimal as possible (Attiny412-chips), and acting mainly as power controllers (motor drivers) and analog to digital conrollers (sensor boards).

The electronics were initially designed using serial TX-RX-connectivity, but eventually I2C-protocol was deemed more appropriate considering the number of devices and required connectivity. The system will be built first with 1 functional 5L growth tank, which will be then expanded to 4 or 6 as wished.

## I2C - devices
The system consists of several devices, each device having its own small board(s)

**Input devices (sensors)**
- pH-meter (1 for each tank) **Manufacturer made amp board + Analog to digital board (I2C)**
- Thermometer (1 for each tank) **Temperature sensor board (I2C)**
- Water level sensor (optional, 1 for reach tank) **Analog to digital board (I2C)**
- Camera (ideally 1 for each tank, otherwise 1 camera with mirrors for tank coverage, (see image below)) **Direct connection to Rasperry Pi**
[![Picture of growth system](../mainproject-img/sensors_option1.png)](../mainproject-img/sensors_option1.png)

[![Picture of growth system](../mainproject-img/sensors_option2.png)](../mainproject-img/sensors_option2.png)

**Outuput devices (controllable devices)**
- LED-growth light (1 for the entire system) **Stepper motor board?(I2C)**, Need to figure out connectivity, most likely through an inbuilt potentiometer
- CO2-valve (1 for each tank) **Solenoid valve board (I2C)**
- Nutrient pumps (1-n for each tank)**DC-motor board (I2C)**
- Water movement pumps (1 for each tank)**DC-motor board (I2C)**
[![Picture of growth system](../mainproject-img/output_option1.png)](../mainproject-img/output_option1.png)

## Device design
The idea is to have some standardised chips with a set function, and inserting new tanks to the system would be as simple as plugging in the new boards and periprehals, setting the addresses and checking the calibration of the periprehals.

This page will come to include drawings and Kicad-files of all the chips used in the system and will explain the design choices made for them



## Programming design
The programming will be set to be as much as possible in the rasperry Pi-end. The chips will only contain basic functionality and I2C-functionality and addressing, and once programmed via UPDI, should not be reprogrammed. The smaller chips will only contain some variables the Rasperry Pi can access to adjust the functionality of the periprehal if absolutely necessary. Otherwise all calibrations and adjustments to the periprehals will be done in Python on the Rasperry Pi. 
