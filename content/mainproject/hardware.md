---
title: Hardware
subtitle:
comments: false
---
###  The Algae growth system

## hardware

This page will come to contain the datasheets and information for all ready-made and bought hardware devices used in the project.

Also, it will include instructions for the initial assembly, calibration and usage/maintanance of the hardware, and a troubleshooting section explaining any/all mistakes and problems I encountered and solutions to them.
