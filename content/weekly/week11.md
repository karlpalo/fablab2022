---
title: Week11 Molding and casting
#subtitle: Why you'd want to hang out with me
#comments: false
---

# Individual Assignments
design a mold around the stock and tooling that you'll be using,
mill it (rough cut + three-axis finish cut),
and use it to cast parts


# design a mold

I desigened a mold in FreeCAD, that would suit a thermometer PCB that I will use for my final project. The idea is that the mold would allow me to encase the part in clear epoxy.

I used a FreeCAD addon called [KiCadStepUp](https://github.com/easyw/kicadStepUpMod), That allows using of the 3D models present in KiCaD-libraries, and import your PCB as a FreeCAD-model.

The only thing to do after installing the add-on, [(via the inbuilt addon-manager)](https://wiki.freecadweb.org/Std_AddonMgr), is to tell FreeCAD, where to find the KiCAD3d models. You can see the image below for the exact filepaths I used.
[![Picture of preferences](../kicadstepup_pref.jpg)](../kicadstepup_pref.jpg)

After that, what happens is pretty cool. Using the addon you can load a Kicad-pcb file (.kicad_pcb), and you will get a 3D model of the part in question! (below is an image of my thermal sensor)
[![Picture of thermal sensor](../kicadstepup_sens.jpg)](../kicadstepup_sens.jpg)

I used this 3d Model then to model a "shell" for my board
[![Picture of shell](../kicadstepup_shell.jpg)](../kicadstepup_shell.jpg)

And then used Boolean tools to remove both the "shell" and the board UPDI connectors I wanted to have not covered by epoxy. (or to be covered by a thin enough layer that could be sanded off later) Furthermore the negative of connectors would ensure the proper placement of the board within the mold, with a thinner part of the epoxy covering the temperature sensor than rest of the board.

[![Picture of mould](../kicadstepup_mould.jpg)](../kicadstepup_mould.jpg)

However, after discussing the issue with kris, I decided to make some modifications to the CAD-file. Firstly, I would make the mold a negative mold, as the silicone used to cast the clear epoxy would have to have a positive mold. This resulted in some redesign of the model (a extra block to account for the wax, and a boolean operation with the original model to create the positive mold). Also kris recommended I would use slanted walls in the edges of the model, as straight walls might cause problems in the milling of the wax (the milling blade passes the same point several times, potentially heating up the wax.).

[![Picture of final model](../kicadstepup_final.jpg)](../kicadstepup_final.jpg)



# Mill it
The milling went nicely, I used v-carve to do a rough milling on the model using a 6 mm flat end tool, and did a finishing path with a 3 mm flat end tool. What was suprising however was the extreme time it took to mill the finishing path. I am not sure if the issue was suboptimal path routing, or what, but milling the finishing paths took almost 1,5 hours! :D  The end result was however worth it!

What is special here, can be seen in the red circle. It is the connector piece I added to the positive mold. (as milling the fine connector piece would have most likely just broken the wax) I measured the location of the connector from a ready made chip, marked it with a knife on the wax (the yellow cross), and then used a hot air gun to heat up the connector (and when pushing it in the wax under it), to create a positive "mold" of the connector.

[![Picture of milled wax](../waxpiece.jpg)](../waxpiece.jpg)


# Cast parts
I ended up with 2 cast parts, first the negative silicon mould, meant for inserting the chip (the connector pins also aligned the chip perfectly and held it in place!).

[![Picture of silicone mold](../silicone.jpg)](../silicone.jpg)

The second part was the epoxy cast of the chip itself see below.
[![Picture of casting](../cast.jpg)](../cast.jpg)
 Everything worked perfectly! Except exposed copper in one place due to an large air bubble in the epoxy.... This can however be fixed with a thin layer of superglue later on


[![Picture of final cast](../chipcast.jpg)](../chipcast.jpg)


## files
[FreeCAD-model of the mould](tempmold_v2.FCStd)
[stl-file of the negative mould](tempmold_v2_negative.stl)
