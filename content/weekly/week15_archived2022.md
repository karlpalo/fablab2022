---
title: Week 15 Networking and communication
#subtitle: Why you'd want to hang out with me
#comments: false
---

## I2C for all
This weeks project for me was to familiarize myself with I2C-protocol,
and see if I could use that for all my device communications.


First of all I configured my rasperry Pi, according to the steps [provided here](https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c). In practice this meant just enabling the I2C protocol for Rasperry Pi and rebooting the device.


I also needed to make a small chip for the rasperry Pi, to introduce pullup between voltage and CLK and CLD-lines. See the small chip below.
[![I2Cpullup](../I2Cpullup.jpg)](../I2Cpullup.jpg) The purpose of this chip is to pull up the SDA and SCL lines to the voltage of 3.3V, as the I2C-protocol uses this voltage to "see" if the connection line is already in use. This small chip is connected to the proper outputs of the Rasperry Pi with connector cables. (I used Pinouts 1,3,5 & 6)
[![Raspinouts](../iRaspberry-Pi-GPIO-Header-with-Photo-1078x516.png)](../Raspberry-Pi-GPIO-Header-with-Photo-1078x516.png)


I also decided to do manual adressing (via UPDI) for each device, as they all use attiny412 chips, the adressing protocol will be as follows

ADDRESS FORMAT : XY (in hexadecimal 0-f)
X = modular growth unit (1-6)
Y = Peripheral devices
  0 = temp sensor
  1 = pH sensor
  2 = empty (light sensor?)
  3 = solenoid gas valve
  4 = water movement motor
  5 = peristaltic pump
  6-f = empty

Adresses 08-0f --> General purpose addresses (e.g. common adjustment for all leds)
Adresses 70-77 --> Extra adresses for bells and whistles

After doing this, I connected the relevant pins from Rasperry Pi to a breadboard I used to "simulate" a serial cable. For one device I managed to get immediate connectivity, and the terminal command


```
i2cdetect -y 1
```


Gave the following output, which corresponded to the arduino code I used for the device (water motor pump)
[![i2ctest](../i2c_test_1.jpg)](../i2c_test_1.jpg)

```
#include <Wire.h>
void setup() {
  Wire.begin(0x14);                // join i2c bus with address #14 (HEXADECIMAL)
}


void loop() {
  delay(100);
}
```
Then I added the pH-sensor board (changing the address in the above code to 11),

[![i2ctest](../i2c_test_3.jpg)](../i2c_test_3.jpg)

 and got the following connectivity signal

[![i2ctest](../i2c_test_2.jpg)](../i2c_test_2.jpg)

## I2C communication in code

Getting the I2C-communitcation to run was suprisingly challenging. First of all, I had to split my development work between two computers.
I still used my PC with windows OS for coding of the attiny412 chips via arduino ide, but for the python backend I had to start using the RassperryPi and its inbuilt Thonny Python IDE. The reason for this is fairly simple, the smbus-package for python (used for I2C communication) has not been ported to windows.

Thus, armed with two computers I challenged the I2C. In retrospect, everything worked from the get go, as I modified the code used in [this tutorial](https://community.element14.com/products/arduino/b/blog/posts/raspberry-pi-and-arduino-i2c-communication).

My problem was that the data transmitted by the attiny412 was strange. I was getting some data from the thermal sensor, but the values I got made no sense. (I expected to get a voltage value of ~512, but instead i got values between 26 and 30 ). I spent quite a lot of time fighting this, but eventually realized what was the problem

In the fighting stage Krisjanis (our instructor at Aalto) recommended i use software serial functionality to remap TX and RX of my SDA and SCL lines to test the functionality in TX/RX-lines via the arduino IDE monitor functionality. In this monitor i saw that the data was transferred as excpected (~512 was ~512), so that lead me to look for alternative reasons, which I finally found in the shape of I2C communication protocol.

I add the testing code for reader interest here. Also, switching between I2C and RX/TX communications allowed me to test and debug the communication protocol really rapidly (I could check via RX/TX that the code produced the expected result and then test the result in I2C)
```
#include <SoftwareSerial.h>
int RX = 2;
int TX = 3;
#define rpin 0    // Built-in LED
#define vpin 1
 SoftwareSerial mySerial(RX, TX); // RX, TX
int voltage = 0;
int vr = 0;
int vm = 0;
byte rnum1,rnum2,rnum3,rnum4; //values of measured voltage are between 1 & 1023
byte mnum1,mnum2,mnum3,mnum4;
 void setup(){

  mySerial.begin(9600);
  mySerial.write("starting");
    pinMode(rpin, INPUT);
  pinMode(vpin, INPUT);
//pinMode(TestPin,OUTPUT);
}
void loop(){

    delay(1000);
    mySerial.write("ref");
    mySerial.print(vr);
    delay(1000);
    mySerial.write("temp");
    mySerial.print(vm);

}
```

### bits and bytes

I2C is configured to send 4 bit data signals (e.g. 0000) and has a 4 bit address header for each data signal (e.g. 0001).

my problems had several causes:
- my attiny412 was sending the temperature as a bit value too large for transport. (512 in binary is 10 0000 0000 --> it would take 2,5 4 bit signals to send this value) ---> I reconfigured the attiny412 code to break down the analog signal (with values 0-1023) into 4 individual numbers, that were then sent via the I2C protocol to rasperry pi

*ARDUINO CODE BLOCK*
```
void loop(){

    delay(1000);
    mySerial.write("ref");
    vr = analogRead(rpin);
rnum4 = (vr) % 10; //rightmost number
rnum3 = (vr / 10) % 10;
rnum2 = (vr / 100) % 10;
rnum1 = (vr / 1000);

    mySerial.print(rnum1);
    mySerial.print(rnum2);
    mySerial.print(rnum3);    
    mySerial.print(rnum4);
    delay(1000);

    mySerial.write("temp");
        vm = analogRead(vpin);
mnum4 = (vm) % 10; //rightmost number
mnum3 = (vm / 10) % 10;
mnum2 = (vm / 100) % 10;
mnum1 = (vm / 1000);


    mySerial.print(mnum1);
    mySerial.print(mnum2);
    mySerial.print(mnum3);    
    mySerial.print(mnum4);

}
```

After this the rasperry pi reacted in more stable way, but it still had some peculiarities, the binary signal I got was in decimal values 48-255 with a zero signal (0000) translating to 48, and line signal (1111) translating to 255. Why was this?
- The answer came when I printed the signal as binary in python. All signals were of form 0011XXXX, which led me to believe that the 0011 was a percursor for the signal. Thus, I wrote an addition to the python code in Rasperry Pi, converting the binary signal to a string, cutting off the precursor, and then converting it back to an integer

*PYTHON CODE BLOCK*
```
# request data
alist = bus.read_i2c_block_data(address, 0,8)
#i2c_block_data reads the message as a 8bit string
# e.g. 00111001 this can be further divided into 2 4 bit strings
# the first 4 bits are for i2c-communication protocol
#the last 4 bits are the actual data being transmitted
blist = list()
for a in alist: #for each item in a-list
    a = '{0:b}'.format(a) # it is made sure the item is in binary format
    a = str(a) #the binary number is converted to a string (for editing)
    a = a[2:6] #the last 4 bits are taken out of the string
    a = int(a,2) # the string is converted back to binary
    blist.append(a) # each item in the initial list appened to a new list to produce the numeric output values

print ("Arduino answer to RPi:", blist)
time.sleep(1)
```

--> in the end I had a working thermal sensor, which
- Measured the voltages of the temperature and voltage sensor 1/s
- Sent these values to Rasperry Pi when called upon via I2C

I could fiddle around with the code, and make the I2C-communication or functionality more refined, but at this point
I think it is overkill, as time is of the essence









## The final codes I used can be found below

ARDUINO CODE BLOCK (For attiny412)
```
/*

 */
#include <Wire.h>
#define SLAVE_ADDRESS 0x10       // I2C address for tempsensor
#define rpin 0    // Built-in LED
#define vpin 1
int i2cData = 0;                 // the I2C data received
int vr = 0;
int vm = 0;
byte rnum1,rnum2,rnum3,rnum4; //values of measured voltage are between 1 & 1023
byte mnum1,mnum2,mnum3,mnum4;
void sendData() {
rnum4 = (vr) % 10; //rightmost number
rnum3 = (vr / 10) % 10;
rnum2 = (vr / 100) % 10;
rnum1 = (vr / 1000);
mnum4 = (vm) % 10; //rightmost number
mnum3 = (vm / 10) % 10;
mnum2 = (vm / 100) % 10;
mnum1 = (vm / 1000);

Wire.print(rnum1);
Wire.print(rnum2);
Wire.print(rnum3);
Wire.print(rnum4);
Wire.print(mnum1);
Wire.print(mnum2);
Wire.print(mnum3);
Wire.print(mnum4);

}
void setup(){
  Wire.begin(SLAVE_ADDRESS);
  Wire.onReceive(receiveData);
  Wire.onRequest(sendData);
  pinMode(rpin, INPUT);
  pinMode(vpin, INPUT);
}
void loop() {
 delay(1000);
    vr = analogRead(rpin);
    vm = analogRead(vpin);



}
// Handle reception of incoming I2C data
void receiveData(int byteCount) {
  while (Wire.available()) {
    i2cData = Wire.read();
    }
  }
// Handle request to send I2C data
```

PYTHON CODE BLOCK (For Rasberry Pi)
```
'''
i2c_v1.py
Simple Python 3 script to test I2C by alternately
sending a 1 over I2C every second and reading
back the response from the Arduino
Tested on Raspberry Pi 4 B+ and Adafruit Feather M0+
'''
import smbus
import time
import sys
bus = smbus.SMBus(1)
time.sleep(1)
address = 0x10              # Sensor I2C Address
def main():
    i2cData = 0
    while 1:
        # send data  (this code block is commented out, it was only used to test if data could be sent to the attiny412)
        #i2cData=2                                                                                                                                      
        #bus.write_byte(address,i2cData)
        # request data
        alist = bus.read_i2c_block_data(address, 0,8)
                    #i2c_block_data reads the message as a 8bit string
                    # e.g. 00111001 this can be further divided into 2 4 bit strings
                    # the first 4 bits are for i2c-communication protocol
                    #the last 4 bits are the actual data being transmitted
        blist = list()
        for a in alist: #for each item in a-list
            a = '{0:b}'.format(a)      # it is made sure the item is in binary format
            a = str(a)                 #the binary number is converted to a string (for editing)
            a = a[2:6]                 #the last 4 bits are taken out of the string
            a = int(a,2)               # the string is converted back to binary
            blist.append(a)            # each item in the initial list appened to a new list to produce the numeric output values

        print ("Arduino answer to RPi:", blist)
        time.sleep(1)
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        gpio.cleanup()
        sys.exit(0)
```
