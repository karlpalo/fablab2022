---
title: Week 19 Invention, IP and Income
#subtitle: Why you'd want to hang out with me
#comments: false
---
##Assingment outline

My plan for the final project is to use it as a scientific apparatus for an upcoming scientific article. It will also act as a basis for a development of a larger-scale (~200L) algal cultivation system, that is currently in development.

For a 1 minute explanatation video of the device, click on the image below.
**TBD**
(Video for last years iteration can be found [here](/fablab2022_H.264.mp4))

The final slide for the project in 2023
**TBD**

(Slide for last years iteration can be found [here](/poster.jpg))

# Licensing

In here I compare 3 different licensing options for the project (and its software).
I will rank them by permissiviness, the most permissive first.

## [MIT-lisence](https://opensource.org/licenses/mit-license.php/)

In essence this licence gives whoever the right to use modify and repurpose the project as they will, as long as the copyright notice is included an the original project is credited. This also allows using this system as a basis for future developments where the project would turn into a closed-source project.

## [GPLv3 - Gnu Public Licence v3](https://www.gnu.org/licenses/gpl-3.0.html)

This license is aimed for keeping things open source, even in future developments. This means that all future applications that use the project as a basis must confirm to this standard. (E.g. usage of parts or the whole of the project in future proprietary closed-source projects is forbidden)


## [LGPL - Gnu Lesser Public License ](http://www.gnu.org/licenses/lgpl-3.0.html)

This license is mostly identical with the GPL, but it allows the use of the software (or parts of it) also in closed source projects. The use of this license is however discouraged by the Gnu-organsation for philosophical reasons. They believe that following the GPL-licensing would benefit the entire open-source community, as it will give it an competetive edge against closed source projects. (Because they are forbidden to use the work for their purposes, unless they go open source)

## [Creative commons 4.0 BY](https://creativecommons.org/licenses/by/4.0/)
This licence is the one Aalto University recommends using for its open access publications. It essentially gives the right for others to use and adapt your work freely, as long as they give appropriate credit, and indicate the changes they have made to the original work. In some cases (if your work contains publicly funded research data), this license is a requirement set by the finnish goverment. For additional info, check the [Aalto website](https://www.aalto.fi/en/open-science-and-research/open-content-licenses-creative-commons) on the issue. However, it should be noted that neither Aalto University or the Creative Commons foundation recommend using this license for software, where GNU or Open Source-licenses are considered to be better applicable.


# Licensing, conclusions

I decided to split the licensing of my project. The software (including the code used to make this website) will go with the GPL3-license, as it keeps even future developments open source.

The hardware and the manufacturing instructions (including the actual informational content of this website) will be covered by the CC 4.0 BY-license.

# Licensing, addendum for academia

If one wants to use Git repositories for publishing here are some tips from Aalto Research data management team.

Git repositories hosted on places like github, gitlab, etc are not suitable for citations simply because they might change the URL, or simply close down.

To have a permanent citation to your software the process is like this:
1. Create a "release" from the current version of your git repository. Github allows to do this with one button press. It basically zips all the current version of the files and you can give it a name such as "1.0".
2. Publish the release zip file into Zenodo, a service that allows self publishing academic outputs such as software. You will get a permanent DOI so that you link the software in your paper, and others will have access to a permanent version of the code, even if github or gitlab will disappear one day.

As I intend to use my device in a an academic publication, I will have to make 2 git repositories (one for the software and one for the hardware), each with a different license. Then create releases of eah and publish them in Zenodo (after which I can refer to them in my academic paper).
