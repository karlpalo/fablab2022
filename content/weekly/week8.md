---
title: Electronics fabrication

#subtitle: Why you'd want to hang out with me
#comments: false
---

## Goal

This week I set myself a relatively ambitious goal, as I wanted to make a hello-world board, that could be used in my final project to control them motor drivers. After talking with Kris I decided to plan a board that could be able to support serial RX/TX-connections with the motor-driver-boards.

For the motor driver, I was planning to use as a basis the board made earlier by kris at (https://gitlab.com/kriwkrow/dc-motor-driver/-/tree/main/)

## What I Did
### Making the board
I made an attiny 1614-based chip, with leds indicating functionality of each future motor, and 1 led indicating the functionality of the board.

The board would also have 2 buttons to test boarrd functionality. (one of these buttons will be later remapped to act as a emenergy shutdown swich for all motors)

Also it will have a 6-pin serial connector for "listening in" to the sent signals using a debugger I made earlier

[![Plan of the board](../board_sch.png)](../board_sch.png)
[![Plan of the board](../board_wir.png)](../board_wir.png)

Making the board proved harder than I initially imagined, I had problems with the glue adhesion of the PCB-board during milling, resulting in several boards with broken traces. (I think the vibration from milling was enough to make the glue flex, making thin traces very prone to breaking off the PCB-board) As a solution I changed the trace width to 0.6 mm (from the initial 0.4mm) and this produced a succesful board. Furthermore, I changed the UPDI-pin to a 6-pin version that is seen in the final board.

Soldering the parts was straitghtforward, and I encountered no problems during that stage of manufacture. Only issue was that my THT-holes for the 2 extra pins were too small for the proper rivet size, so I had to attach the 2 extra pin socket just via surface soldering.

Despite all the great talks here, I eventually noticed that the buttons were misconnected (connected to VCC instead of ground), and I managed to burn out the chip as a consequence. Thus I ended up doing another, slightly simpler version of the board (seen below)

[![Plan of the board](../board2_sch.png)](../board2_sch.png)
[![Plan of the board](../board2_wir.png)](../board2_wir.png)

### Getting started with programming

 First I installed arduino IDE 1.8.19, and then downloaded the proper libraries ([Attiny](https://github.com/SpenceKonde/ATTinyCore/blob/master/Installation.md) and [Atmega](https://github.com/SpenceKonde/megaTinyCore)-libraries for 412 and 1614 respectively)

 However, after plugging in the board, there was no reaction from my computers COM-port. After hours of debugging I realized that the installation had been incomplete (I had lost wi-fi access during installation). Reinstallation of the libraries fixed the problems (I had multiples of extremely erratic problems!)

 After fixing the code (and taking the chip vestion 2 to use) everything worked from the first upload of the code. (as you can see in the video below)
[![Video of the board](../board2_flashled.png)](../board2_flashled.mp4)


 Eventually I used [hello Train!](https://atmega32-avr.com/hello-train-attiny-1614/) as a basis for my own code.

 ```
 // Karl MIhhels, fab academy, digital fabrication II
 //
 //Original code:Neil Gershenfeld 12/8/19
 // This work may be reproduced, modified, distributed,
 // performed, and displayed for any purpose, but must
 // acknowledge this project. Copyright is retained and
 // must be preserved. The work is provided as is; no
 // warranty is provided, and users accept all liability.
 //

 const int ledPin1 = 0;   //button1 light
 const int ledPin2 = 1;   //button1 light
 const int ledPin3 = 2;   //button2 light
 const int ledPin4 = 3;   //button2 light

 const int button1Pin = 9;
 const int button2Pin = 8;
 int button1State = 0;
 int button2State = 0;


 void setup() { //declaration of inputs and outputs

   pinMode(ledPin1, OUTPUT);
   pinMode(ledPin2, OUTPUT);
   pinMode(ledPin3, OUTPUT);
   pinMode(ledPin4, OUTPUT);
   pinMode(button1Pin, INPUT);
   pinMode(button2Pin, INPUT);

 }
 void loop() {

   button1State = digitalRead(button1Pin);
   button2State = digitalRead(button2Pin);
   // check if the pushbutton is pressed. If it is, the buttonState is LOW (as it is connected to GND):
   // Button1 loop
   if (button1State == LOW) {
     // turn LED on:
     digitalWrite(0, HIGH);
     digitalWrite(1, HIGH);
   } else {
     // turn LED off:
     digitalWrite(0, LOW);
     digitalWrite(1, LOW);
   }
 // Button2 loop

   if (button2State == LOW) {
     // turn LED on:
     digitalWrite(2, HIGH);
     digitalWrite(3, HIGH);
   } else {
     // turn LED off:
     digitalWrite(2, LOW);
     digitalWrite(3, LOW);
   }
 }
```



## What worked


## What did not

The board version 2 had erratic led behaviour. (The leds lit sup sometimes on their own) After reading on the topic I learned this was the result of lacking an pull-up resistor. This means that the buttons press down reference level (GND) is not referenced to anything. [To fix it I would need to add a pull-up resistor between the VCC and and the button PIN, to "tell" the chip what is HIGH-voltage. ](https://learn.sparkfun.com/tutorials/pull-up-resistors/all)

To remedy the situation, The fix would be to add a extra wire connecting the button pinouts (with a sufficient resistor, e.g. 50K ohm.) to the VCC. However, Attiny 1614 has an internal pullup-option which can be activated by replacing the code

```
pinMode(button1Pin, INPUT);
pinMode(button2Pin, INPUT);
```

width

```
pinMode(button1Pin, INPUT_PULLUP);
pinMode(button2Pin, INPUT_PULLUP);
```
Eliminating the erratic behaviour

## Cheking of the digital signal from artduino uno

To test the oscilloscope I decided to take an Arduino Uno board, and program it to send byte messages "11" and "01"

Results were succesful, largely in part due to the advanced automatic functionality of our particular oscilloscope, which helped in isolating the timing window of the message easily. (As there was automatic detection for wavefront start)

### code for 11 message
```
void setup() {
  Serial.begin(9600);


}

void loop() {
  Serial.println(0b11, BIN); // the 0b prefix indicates a binary constant
  delay(100);

}
```
The code above resulted in the following signal
[![result of the 11 message](../byte11.jpg)](../byte11.jpg)


### code for 01 message
```
void setup() {
  Serial.begin(9600);


}

void loop() {
  Serial.println(0b11, BIN); // the 0b prefix indicates a binary constant
  delay(100);

}
```
The code above resulted in the following signal

[![result of the 01 message](../byte01.jpg)](../byte01.jpg)
