---
title: Week2 Assignment
#subtitle: Why you'd want to hang out with me
#comments: false
---

This is the week 3 assignment of my fablab-project

As this week was part of the Digital fabrication I course in 2021, the work is documented in
[https://karlpalo.gitlab.io/fablab2021//assignments/assignment-03/](https://karlpalo.gitlab.io/fablab2021//assignments/assignment-03/)
