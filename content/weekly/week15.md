---
title: Week 15 Networking and communication
#subtitle: Why you'd want to hang out with me
#comments: false
---

## Assingnment outline
Follow the steps below to complete your assignment.

    Explore the WiFi capabilities of the XIAO ESP32C3 board.
    Plug it into one of the boards you produced earlier.
    Program the XIAO board to spawn an Access Point and create a simple REST API.
    The REST API should have endpoints for controlling output device(s) of your board as well as reading input.
    Document as you go and publish content on a page on your documentation website.

## comments
My assingnment will be a bit different, as I use I2C communication instead of the wireless REST API. (This is more in line with Digital fabrication for 2022). This section does demonstrate all the same skills in regards building data networks and communication.


## I2C

This weeks project is essentially a connection of Rasperry PI to a I2C bus of 3 identical featherboards

Both the boards and the Rpi are powered by external 5V power supply. Thus they share ground and 5V-voltage levels.
However, as I2C operates on 3.3V both RPi and Featherboards contain internal voltage adjusters that set the logic level voltage to this value. For good I2C-signal quality I noticed that I had to also connect the 3.3V voltages to a shared bus. If the logic level voltages were
disconnected, the data transfer of I2C was notably more unstable (More info in the section on data validation)


## Software libraries
### For Featherboard (Arduino)
For the featherboard, 3 libraries are being used;
[Wire.h](https://www.arduino.cc/reference/en/language/functions/communication/wire/) - For I2C-communication
[Onewire.h](https://www.pjrc.com/teensy/td_libs_OneWire.html) - For thermal sensor OneWire-protocol
[DallasTemperature.h](https://github.com/milesburton/Arduino-Temperature-Control-Library) - Protocols for the specific brand of thermal sensor we use

Of these libraries, the Wire.h is of interest for us. Below is the minimal code for wire.h used in my project featherboards

The I2C is used not with the "void loop()" - function, as it is left empty, instead two custom functions are defined
"recieveData" & "sendaData"

recieveData - function recieves commands from RPi (e.g. what valves to close or open in the CO2-dosing)
Commands are of format 1ABC, where 1 is a byte checking the signal quality and it is follwed by ABC, where each letter has value of 0 or 1 denoting valve status. For signal quality checking, all data recieved in incorrect format results in a serial-based error flag, and closing of all CO2-valves (until next update command).

sendaData- function is named send*a*Data due to the fact that function name "sendData" is already being used by arduino Library
The sendData-funtion reads the temperature sensor, 3 pH-sensors, converts the read data to bytes via the inttobinary function.
also, each dataset is preceded by the bytes corresponding to 1234 and ending with bytes corresponding to 4321.

The arduino code is presented below

```
#include <Wire.h>
#define SLAVE_ADDRESS 0x08       // I2C address for Arduino
#define LED 13                   // Built-in LED - Just for debugging
#define refhigh 5               //reference voltage for rpi (logic level voltage)

 // START OF TEMPSENSOR CODE
#include <OneWire.h>
#include <DallasTemperature.h>
// Data wire is connected to the Arduino analog pin MISO
#define ONE_WIRE_BUS MISO
#define PHSENSOR1 A3
#define PHSENSOR2 A2
#define PHSENSOR3 A1
#define PHSENSOR4 A0
#define VALVE1 13
#define VALVE2 12
#define VALVE3 11
#define VALVE4 10


int i2cData = 0;                 // the I2C data received
byte ph1low;
byte ph1high;
int testint = 1234;
int number = 0;
int num1 = 0;
int num2 = 0;
int num3 = 0;
int num4 = 0;
int t1 = 0;
int ph1 = 0;
int ph2 = 0;
int ph3 = 0;

void inttobinary(int number){
  num4 = (number) % 10; //rightmost number
  num3 = (number / 10) % 10;
  num2 = (number / 100) % 10;
  num1 = (number / 1000);
  Wire.write(num1);
  Wire.write(num2);
  Wire.write(num3);
  Wire.write(num4);

}


void setup(){
  Wire.begin(SLAVE_ADDRESS);
  Wire.onReceive(receiveData);
  Wire.onRequest(sendaData);
  pinMode(LED, OUTPUT);
  digitalWrite(LED, HIGH);
  pinMode(refhigh, OUTPUT);
  digitalWrite(refhigh,HIGH);
}
void loop() {

  sensort1.requestTemperatures();
  t1 = sensort1.getTempCByIndex(0);

  ph1 = analogRead(PHSENSOR1);
  ph2 = analogRead(PHSENSOR2);
  ph3 =analogRead(PHSENSOR3);



}
// Handle reception of incoming I2C data
void receiveData(int byteCount) {
  Serial.println(byteCount);
  i2cdata = 0;
  while (byteCount > 0) {
    i2cdatatemp = Wire.read();
    i2cdatatemp = i2cdatatemp & B001111;
    powerstorage = pow(10,byteCount-1);
    i2cdatatemp = i2cdatatemp*powerstorage;
    i2cdata = i2cdata + i2cdatatemp;   
    byteCount = byteCount-1;

}
if (i2cdata == 0000){
  Serial.println("potato");


}
else if (i2cdata == 1000){
  digitalWrite(VALVE1, LOW);
  digitalWrite(VALVE2, LOW);
  digitalWrite(VALVE3, LOW);
  Serial.println("000");
}
else if (i2cdata == 1001){
  digitalWrite(VALVE1, LOW);
  digitalWrite(VALVE2, LOW);
  digitalWrite(VALVE3, HIGH);
  Serial.println("001");
}
else if (i2cdata == 1010){
  digitalWrite(VALVE1, LOW);
  digitalWrite(VALVE2, HIGH);
  digitalWrite(VALVE3, LOW);
  Serial.println("010");
}
else if (i2cdata == 1011){
  digitalWrite(VALVE1, LOW);
  digitalWrite(VALVE2, HIGH);
  digitalWrite(VALVE3, HIGH);
  Serial.println("011");
}
else if (i2cdata == 1100){
  digitalWrite(VALVE1, HIGH);
  digitalWrite(VALVE2, LOW);
  digitalWrite(VALVE3, LOW);
  Serial.println("100");
}
else if (i2cdata == 1101){
  digitalWrite(VALVE1, HIGH);
  digitalWrite(VALVE2, LOW);
  digitalWrite(VALVE3, HIGH);
  Serial.println("101");
}
else if (i2cdata == 1101){
  digitalWrite(VALVE1, HIGH);
  digitalWrite(VALVE2, HIGH);
  digitalWrite(VALVE3, LOW);
  Serial.println("110");
}
else if (i2cdata == 1111){
  digitalWrite(VALVE1, HIGH);
  digitalWrite(VALVE2, HIGH);
  digitalWrite(VALVE3, HIGH);
  Serial.println("111");
}
else {
  digitalWrite(VALVE1, LOW);
  digitalWrite(VALVE2, LOW);
  digitalWrite(VALVE3, LOW);
  Serial.println("Unexpected input, closing all valves");
  Serial.println(i2cdata);
}
}  


// Handle request to send I2C data - with a 1234 preamble and 4321 ending
void sendaData() {
inttobinary(1234);
inttobinary(t1);
inttobinary(ph1);
inttobinary(ph2);
inttobinary(ph3);
inttobinary(4321);
}

```
### The RPi-libraries and code  

The RPi-backend communicating with the Featherboard uses a library called [smbus2](https://github.com/kplindegaard/smbus2) as the library to manage I2C-communication. It should be noted that smbus, (and most other openly available I2C-libraries) do not strictly adhere to the I2C-standard, but are in fact a software simulation of a protocol practically identical to I2C. Ultimately it is a question of signal interpretation, and for advanced stuff such as high-speed I2C, more specialized signal reading libraries might be needed.

the python library "database" is just a collection of functions written on top of SQLite3-python library that take the raw data from the sensors and input them into a SQL-table for storage. The library also reads data from SQL-table to decide wheter to toggle the CO2-valves on and off


```

import smbus2
import time
import sys
import database
bus = smbus.SMBus(1)
address1 = 0x08            # Featherboard #1 I2C Address
address2 = 0x09            #  Featherboard #2 I2C Address
address3 = 0x10             #  Featherboard #3 I2C Address

sens1 = 0 #ph sensor 1 value will come here
sens2 = 0 #ph sensor 2 value will come here
sens3 = 0 #ph sensor 3 value will come here
sens4 = 0 #temp sensor 1 value will come here
sens5 = 0 #ph sensor 4 value will come here
sens6 = 0 #ph sensor 5 value will come here
sens7 = 0 #ph sensor 6 value will come here
sens8 = 0 #temp sensor 2 value will come here
sens9 = 0 #ph sensor 7 value will come here
sens10 = 0 #ph sensor 8 value will come here
sens11 = 0 #ph sensor 9 value will come here
sens12 = 0 #temp sensor 3 value will come here

# This function takes the byte-input from smbus2
# and returns it in a nice format for further processing
#(a set of numbers 4*12 long)
def inputcleaner(inputdata):
    inputdata = list(inputdata)
    for item in range(len(inputdata)):
        inputdata[item] = int(inputdata[item])
        if inputdata[item] > 9:
            inputdata[item] = 0
        else:
            inputdata[item] = inputdata[item]
    inputdata = str(inputdata)
    inputdata = inputdata.replace(' ','')
    inputdata = inputdata.replace(',','')
    inputdata = inputdata.replace('[','')
    inputdata = inputdata.replace(']','')
    return inputdata

# Just an assisting function to slice strings efficiently
def splitter(string,splitlength):  
    return[string[i:i +splitlength ] for i in range(0,len(string),splitlength)]

# This function checks that the I2C-signal recieved is valid
# This is an ad hoc-validation cheking that the first and last bytes are 1234 & 4321
# If there is an error in the signal, the signal gets set to 0
# A pH signal of 0 is the lowest possible pH --> the valve will snap shut.
def inputchecker(inputdata):
    startkey = inputdata[0:4]
    endkey = inputdata[24:28]
    if startkey == "1234" and endkey == "4321":
       inputdata = inputdata[4:24]
       inputdata = splitter(inputdata,4)
       return inputdata
    else:
       print(inputdata)
       inputdata = "0000000000000000"
       inputdata = splitter(inputdata,4)
       return inputdata

#this is just a small helper function
def inputprocesser(inputdata):
        inputdata = inputcleaner(inputdata)
        inputdata = inputchecker(inputdata)
        return inputdata


#this is just a small helper function
def valvechecker(state):
        if state == 'open':
            return 1
        else:
            return 0


def main():
    i2cData08 = False
    while 1:
        #fetch the latest of valve (open/closed) commands from database

        a08 = database.return_latest_commands_item(9)
        b08 = database.return_latest_commands_item(10)
        c08 = database.return_latest_commands_item(11)
        i2cData08 = [valvechecker(a08),valvechecker(b08),valvechecker(c08)]

        a09 = database.return_latest_commands_item(12)
        b09 = database.return_latest_commands_item(13)
        c09 = database.return_latest_commands_item(14)
        i2cData09 = [valvechecker(a09),valvechecker(b09),valvechecker(c09)]       

        a10 = database.return_latest_commands_item(15)
        b10 = database.return_latest_commands_item(16)
        c10 = database.return_latest_commands_item(17)
        i2cData10 = [valvechecker(a10),valvechecker(b10),valvechecker(c10)]        

        print(i2cData08)

        #write the valve data to the Featherboards, the sleep is there to ensure that there is no overlap in the signals
        bus.write_i2c_block_data(address1,5,i2cData08)
        time.sleep(0.1)
        bus.write_i2c_block_data(address2,5,i2cData09)
        time.sleep(0.1)
        bus.write_i2c_block_data(address3,5,i2cData10)
        time.sleep(0.1)

        #Reads sensors and fills in the database with the latest sensor data
        chip08 = bus.read_i2c_block_data(0x08,0,28)
        chip09 = bus.read_i2c_block_data(0x09,0,28)
        chip10 = bus.read_i2c_block_data(0x10,0,28)
        time.sleep(0.1)
        #clean the data
        chip08 = inputprocesser(chip08)
        chip09 = inputprocesser(chip09)
        chip10 = inputprocesser(chip10)


        #process the data to the form that MSQLite wants it
        sensorinputs = chip08[1:4]
        sensorinputs.extend(chip09[1:4])
        sensorinputs.extend(chip10[1:4])
        sensorinputs.extend(chip08[0:1])
        sensorinputs.extend(chip09[0:1])
        sensorinputs.extend(chip10[0:1])
        sens1 = sensorinputs[0]
        sens2 = sensorinputs[1]
        sens3 = sensorinputs[2]
        sens4 = sensorinputs[3]
        sens5 = sensorinputs[4]
        sens6 = sensorinputs[5]
        sens7 = sensorinputs[6]
        sens8 = sensorinputs[7]
        sens9 = sensorinputs[8]
        sens10 = sensorinputs[9]
        sens11 = sensorinputs[10]
        sens12 = sensorinputs[11]

        database.add_all_sensordata(sens1,sens2,sens3,sens4,sens5,sens6,sens7,sens8,sens9,sens10,sens11,sens12)
        #Update valvevalues
        database.updatevalvestate()


        #just a check function
        print ("data read from feather1:", chip08)
        print ("data read from feather2", chip09)
        print ("data read from feather3:", chip10)

        #just a check function
        print ("temperature from feather1 in C:", sens4)
        print ("temperature from feather2 in C:", sens8)


# set the refresh rate for a single update of the system (data reading + response)
        time.sleep(1)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        gpio.cleanup()
        sys.exit(0)


```


## Basic communication and connectivity demo
The boards were connected according to the wiring pattern shown in the image below. all of these coloured lines are in fact BUS-type connections.
- 5V *red*
- 3,3V *green* (This is set in featherM0 by setting pin 5 HIGH in the code)
- Ground *brown*
- I2C Data *orange*
- I2C Clock *yellow*
[![Picture of I2C connections](../I2C_connection.png)](../I2C_connection.png)

After all the connections were made, and the Arduino code uploaded to to the feather M0:s, I could check the connectivity by inserting the following command in RPi command console
```
i2cdetect -y 1
```

[![Picture of I2C connections](../I2C_connected.png)](../I2C_connected.png)

In this picture you see that 2 featherboards (with addresses 08 and 10) are connected.

And when running the Backend.py on RPi, the following output is produced. ( The update interval was set to 5 seconds in this example)

[![Picture of I2CData ](../I2C_Shell.png)](../I2C_Shell.png)


## Add the necessary Files
- [UI.py](UI.py)
- [Backend.py](../featherbackend.py)
- [Database.py](../database.py) (For the SQLite management)
- [Featherboard - Arduino code](../featherm0.ino)
