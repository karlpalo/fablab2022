'''
i2c_v1.py
Simple Python 3 script to test I2C by alternately
sending a 1 and 0 over I2C every second and reading
back the response from the Arduino
Tested on Raspberry Pi 4 B+ and Adafruit Feather M0+
'''
import smbus
import time
import sys
import database
bus = smbus.SMBus(1)
address1 = 0x08            # Arduino I2C Address
address2 = 0x09
address3 = 0x10

sens1 = 0
sens2 = 0
sens3 = 0
sens4 = 0
sens5 = 0
sens6 = 0
sens7 = 0
sens8 = 0
sens9 = 0
sens10 = 0
sens11 = 0
sens12 = 0


def inputcleaner(inputdata):
    inputdata = list(inputdata)
    for item in range(len(inputdata)):
        inputdata[item] = int(inputdata[item])
        if inputdata[item] > 9:
            inputdata[item] = 0
        else:
            inputdata[item] = inputdata[item]
    inputdata = str(inputdata)
    inputdata = inputdata.replace(' ','')
    inputdata = inputdata.replace(',','')
    inputdata = inputdata.replace('[','')
    inputdata = inputdata.replace(']','')
    return inputdata

def splitter(string,splitlength):
    return[string[i:i +splitlength ] for i in range(0,len(string),splitlength)]

def inputchecker(inputdata):
    startkey = inputdata[0:4]
    endkey = inputdata[24:28]
    if startkey == "1234" and endkey == "4321":
       inputdata = inputdata[4:24]
       inputdata = splitter(inputdata,4)
       return inputdata
    else:
       print(inputdata)
       inputdata = "0000000000000000"
       inputdata = splitter(inputdata,4)
       return inputdata
        
def inputprocesser(inputdata):
        inputdata = inputcleaner(inputdata)
        inputdata = inputchecker(inputdata)
        return inputdata
    
def valvechecker(state):
        if state == 'open':
            return 1
        else:
            return 0


def main():
    i2cData08 = False
    while 1:
        # send the latest set of valve (open/closed) from database
        
        a08 = database.return_latest_commands_item(9)
        b08 = database.return_latest_commands_item(10)
        c08 = database.return_latest_commands_item(11)
        i2cData08 = [valvechecker(a08),valvechecker(b08),valvechecker(c08)]
        
        a09 = database.return_latest_commands_item(12)
        b09 = database.return_latest_commands_item(13)
        c09 = database.return_latest_commands_item(14)
        i2cData09 = [valvechecker(a09),valvechecker(b09),valvechecker(c09)]       
        
        a10 = database.return_latest_commands_item(15)
        b10 = database.return_latest_commands_item(16)
        c10 = database.return_latest_commands_item(17)
        i2cData10 = [valvechecker(a10),valvechecker(b10),valvechecker(c10)]        
        
        print(i2cData08)
        bus.write_i2c_block_data(address1,5,i2cData08)
        time.sleep(0.1)
        bus.write_i2c_block_data(address2,5,i2cData09)
        time.sleep(0.1)
        bus.write_i2c_block_data(address3,5,i2cData10)
        time.sleep(0.1)
        
        #Read sensors and fill in the database with the latest sensor data
        chip08 = bus.read_i2c_block_data(0x08,0,28)
        chip09 = bus.read_i2c_block_data(0x09,0,28)
        chip10 = bus.read_i2c_block_data(0x10,0,28)
        chip08 = inputprocesser(chip08)
        chip09 = inputprocesser(chip09)
        chip10 = inputprocesser(chip10)
        
        sensorinputs = chip08[1:4]
        sensorinputs.extend(chip09[1:4])
        sensorinputs.extend(chip10[1:4])
        sensorinputs.extend(chip08[0:1])
        sensorinputs.extend(chip09[0:1])
        sensorinputs.extend(chip10[0:1])
        sens1 = sensorinputs[0]
        sens2 = sensorinputs[1]
        sens3 = sensorinputs[2]
        sens4 = sensorinputs[3]
        sens5 = sensorinputs[4]
        sens6 = sensorinputs[5]
        sens7 = sensorinputs[6]
        sens8 = sensorinputs[7]
        sens9 = sensorinputs[8]
        sens10 = sensorinputs[9]
        sens11 = sensorinputs[10]
        sens12 = sensorinputs[11]

        database.add_all_sensordata(sens1,sens2,sens3,sens4,sens5,sens6,sens7,sens8,sens9,sens10,sens11,sens12)
        #Update valvevalues
        database.updatevalvestate()
        
        
        
        
        print ("Arduino answer to RPi:", chip08)
        print ("Arduino answer to RPi:", chip09)
        print ("Arduino answer to RPi:", chip10)


        
#database.add_all_sensordata(
    
        time.sleep(5)
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        gpio.cleanup()
        sys.exit(0)
        