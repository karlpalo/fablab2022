---
title: Week13 Machine building
#subtitle: Why you'd want to hang out with me
#comments: false
---
# The Machine
The machine we built was a kitty litter poop scooper.
The details of the machine can be found in the relevant git at
[https://gitlab.com/aaltofablab/machine-building-2022b](https://gitlab.com/aaltofablab/machine-building-2022b)

## My contribution
My contribution was in the initial design of the mechanics
[![Design](../design.png)](../design.png)

And scrounging the labs for the materials and parts for the initial beta-prototype of the machine. Also tried my hand on needlework, as I assembled a few custom-size belts for the prototype.

Basically my work can be seen in this picture below. All the belt tighteners, belts, gears, rods and the design of the assembly was my work

[![chassis](../chassis.jpg)](../chassis.jpg)

However, in the final version of the machine my contribution was quite small. Just did some assembly work and worked on the general documentation. The whole process raised a question in my mind for the git. How should one treat the early prototypes? Should they be documented as well? (And the design flaws discovered and fixed for the final version)
