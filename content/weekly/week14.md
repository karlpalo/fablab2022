---
title: Week14 Input devices
#subtitle: Why you'd want to hang out with me
#comments: false
---

## looking at a two sensors, pH and Temperature

My system has 2 main sensors, one for pH, the other for temperature.
The temperature sensor is completely self made, and is expected to work fine with both a voltage of 3.3 V and 5.0 V, as the thermistor retains essentially identical functionality in both cases. (However, once calibrated to a certain voltage, shifting the voltage might offset the calibration)

The pH-sensor is a bit more tricky. It is a [V1 DF-robot pH-sensor](https://www.dfrobot.com/product-1025.html), and the main difference, when compared to V2 of the sensor, is that it is rated only for 5.0 V, as the version 2 of the sensor can accept both input voltages.


For sensors themselves, I used essentially identical codes, as I only wanted the digital signal of the raw voltage measured by the chip connected to the sensor. The only difference is that the temperature sensor contains two 4 byte codes, vr and vm. ( vr is standing for reference voltage, measured between 2 1K resistors, while vm stands for the voltage measured between a 1K resistor and the thermistor) The actual conversion of the voltage to a real temperature, and the calibration will be done in the python code in rasperry Pi.

I however had to do a bit of byte magic (breaking the base 10 signal sent by arduino code into individual bytes in base 2 (0000-1111)). The reason for this was not in the sensors, but instead in the I2C-communication protocol I used for my project. [See next weeks work for details](https://karlpalo.gitlab.io/fablab2022/weekly/week15/)




## pH-sensor

As you can see from the image below, the pH-sensor came with its own chip. The whole sensor setup thus contains 3 parts. The pH-probe, that is immersed in water. The signal amplifier chip, provided by DF-robot along the sensor, and finally my own attiny412 chip reading the signal voltage, and converting it into digital signal for I2C.  As signal amplification and filtering is a jungle I did not want to start exploring, I decided to trust the manufacturer, and not make my own signal amplification/filtering circuit for pH-probe.

[![ph-Sensor](../phsens.jpg)](../phsens.jpg)


I initially tried running the pH-sensor on 3.3 V, but during calibration I noticed that the response to pH-change was extremely non-linear, and most of the sensivity range was not used. (E.g. the analogRead-function gave values over 800 on all pH measurement points [4,7,10], with pH 7 and ph 10 giving nearly identical readings [~900 and ~930 respectively]) With 5.0 V the sensor behaved in a much more stable way, with the reading at pH 7 being at the middle of the range of analogRead-function (value of ~ 600 ).

**However, I needed to make a level-shifter, as connecting a chip with 5.0 V logic into the I2C running at 3.3 V would have destroyed the rasperry Pi!**
In this I used as a reference (after asking for advice) [the work Krisjanis did during his own fab academy work](http://fabacademy.org/2018/labs/barcelona/students/krisjanis-rijnieks/assignments/week14/), and modified his level shifter a bit to suit my purposes. (E.g. to shift 2 lines instead of 1)
[![levelshifter](../levelshifter.jpg)](../levelshifter.jpg)

Another thing I noticed was that if the black analog cable, between the probe and signal amplifier was moved, the measured voltage changed, even if the actual pH of the water the probe was immersed in remained same. Thus, I knew I would need to plan a holder for the pH-probe, that would allow for relatively good stabilization of the analog signal cable, if I wanted the measurements to have some degree of accuracy.



### Attiny 412 code

```
/*
 * Arduino code to send and receive I2C data
 * Tested on Adafruit Feather M0+ Express and Raspberry Pi Model 4
 *
 * SDA <--> SDA
 * SCL <--> SCL
 * GND <--> GND
 *
 */
#include <Wire.h>
#define SLAVE_ADDRESS 0x11       // I2C address for pHsensor
#define pHpin 0
int i2cData = 0;                 // the I2C data received
int pHvolt = 0;

byte pHnum1,pHnum2,pHnum3,pHnum4; //values of measured voltage are between 1 & 1023


// Handle request to send I2C data
void sendData() {
pHnum4 = (pHvolt % 10); //rightmost number
pHnum3 = (pHvolt / 10) % 10;
pHnum2 = (pHvolt / 100) % 10;
pHnum1 = (pHvolt / 1000);


Wire.print(pHnum1);
Wire.print(pHnum2);
Wire.print(pHnum3);
Wire.print(pHnum4);


}
void setup(){
  Wire.begin(SLAVE_ADDRESS);
  Wire.onReceive(receiveData);
  Wire.onRequest(sendData);
  pinMode(pHpin, INPUT);

}
void loop() {
 delay(1000);
    pHvolt = analogRead(pHpin);


}
// Handle reception of incoming I2C data
void receiveData(int byteCount) {
  while (Wire.available()) {
    i2cData = Wire.read();
    }
  }

```


## temperature sensor

The temperature sensor worked nicely out of the box. Just plug and play!

### Attiny 412 code

```
/*
 * Arduino code to send and receive I2C data
 * Tested on Adafruit Feather M0+ Express and Raspberry Pi Model 4
 *
 * SDA <--> SDA
 * SCL <--> SCL
 * GND <--> GND
 *
 * Sets built-in LED (1 = on, 0 = off) on Feather when requested
 * and responds with data received
 */
#include <Wire.h>
#define SLAVE_ADDRESS 0x10       // I2C address for tempsensor
#define rpin 0    // Built-in LED
#define vpin 1
int i2cData = 0;                 // the I2C data received
int vr = 0;
int vm = 0;
byte rnum1,rnum2,rnum3,rnum4; //values of measured voltage are between 1 & 1023
byte mnum1,mnum2,mnum3,mnum4;


// Handle request to send I2C data
void sendData() {
rnum4 = (vr) % 10; //rightmost number
rnum3 = (vr / 10) % 10;
rnum2 = (vr / 100) % 10;
rnum1 = (vr / 1000);
mnum4 = (vm) % 10; //rightmost number
mnum3 = (vm / 10) % 10;
mnum2 = (vm / 100) % 10;
mnum1 = (vm / 1000);

Wire.print(rnum1);
Wire.print(rnum2);
Wire.print(rnum3);
Wire.print(rnum4);
Wire.print(mnum1);
Wire.print(mnum2);
Wire.print(mnum3);
Wire.print(mnum4);

}
void setup(){
  Wire.begin(SLAVE_ADDRESS);
  Wire.onReceive(receiveData);
  Wire.onRequest(sendData);
  pinMode(rpin, INPUT);
  pinMode(vpin, INPUT);
}
void loop() {
 delay(1000);
    vr = analogRead(rpin);
    vm = analogRead(vpin);



}
// Handle reception of incoming I2C data
void receiveData(int byteCount) {
  while (Wire.available()) {
    i2cData = Wire.read();
    }
  }
```
## Files for the sensors PCB:s in Kicad

[Level Shifter](levelshifterI2C.zip)
[Ph-sensor, analog to digital converter](PHsensor_I2C.zip)
[temperature sensor](Tempsensor_I2C.zip)
