---
title: Week1 Assignment
#subtitle: Why you'd want to hang out with me
#comments: false
---

This is the week 1 assignment of my fablab-project

As this week was part of the Digital fabrication I course in 2021, the work is documented in
[https://karlpalo.gitlab.io/fablab2021//assignments/assignment-01/](https://karlpalo.gitlab.io/fablab2021//assignments/assignment-01/)
