---
title: Week 18 Applications and Implications
#subtitle: Why you'd want to hang out with me
#comments: false
---

## Final Project

My final project is an experimental device capable of supporting 18 simultaneous growth experiments, with 9 of them containing active pH-monitoring and adjustment via CO2-valves and diffusers

Below is a section by section description how this final project utilizes the skills I have learned during digital fabrication courses, ending with a Bill of materials for the current iteration of the project.

## 2D design and substractive fabrication

The current iteration of the system does not incorporate much 2D-design, but the housing of the previous iteration of the growth system is a good exampe of both 2D-design and substractive fabrication (CNC-milling). [The full description for the manufacturing of this part can be found in previous years documentation.](https://karlpalo.gitlab.io/fablab2022/weekly/week9/)

[![chassis](../chassis.png)](../chassis.png)
[![chassisCAD](../chassisCAD.png)](../chassisCAD.png)

The design files for this part can be found [here.](../algaetankholder_ver2.FCStd)

## 3D design and additive fabrication

The project is housed mainly in [commercial plastic boxes](https://www.clasohlson.com/fi/SmartStore-Home-S&auml;ilytyslaatikko-kannella/p/34-1552-15), with holes made for cabling. The reason for this was the quickness of this solution and the fact that the exact design of the system was open to small changes up to the very last stages. In future versions more sophisticated housing could and should be developed.

3D design was used for several parts, with one of the more challenging ones being a simple adapter for CO2-tubing. The adapter had several engineering requirements; it had to tolerate moderate pressures without leaking (up to 5 bar), and fit seamlessly to facilitate connection between a 2mm inner diameter silicone tube, and 4mm inner diameter hard plastic tube.

### 3D-design and additive fabrication (3D-printing)

To find the right plastic i had to consult [chemical resistance charts from commercial suppliers](https://visserssales.com/wp-content/uploads/2021/08/Chemical_Chart.pdf). Luckily ABS seemed to have decent performance, and it was available for me as a 3D-printin material. A further davantage of ABS is also that it can be melted with acetone vapours, and I used this acetone vapour melting to ensure that there were no gas leaks between the 3D-printed layers. (as if there was a small gap between 2 layers, the liquid ABS melted by the acetone could be expected to fill the gaps)

[![picture of tube adapter](../adapter.png)](../adapter.png)

This small piece required a suprisingly sophisticated design with roundings for strength.

[![used roundings](../rounding.png)](../rounding.png)

And taperings to ensure snug fits to the tubing (especially the rigid 4mm one)
[![used roundings](../thickening.png)](../thickening.png)

No detailed engineering data on pressure resistance of 3D-printed abs-plastics, so I tried to make ensure that the plastic part was overtly strong for the purpose. I also did a test pressuring at 8 bar, to ensure the robustness of the part. As the part survived the test pressuring with no damage, this design was taken to use in my growth system.

To further improve the sealing properties, the ABS-printed part was printed with a fill rate of 100 %, exposed to acetone vapours to melt the surfaces slightly and ensure good sealing, and finally the connection surfaces were secured with a thin layer of[parafilm](https://www.mekalasi.fi/tuote/parafilm-m-10-cm-x-38-m/) to ensure complete sealing.

##     Electronics design and production

The project uses for processing electronics commerial fether M0-boards for hardware control, and a Rasperry Pi to control the feather M0-boards and to contain all the software. More sophisticated self-made electronics have been used in previous iterations of the project, such as a motor driver chip presented [here](https://karlpalo.gitlab.io/fablab2022/weekly/week12/)

The current iteration of the project however contains a set of quite nifty trigger-chips that are used to trigger the 5V gas valves. This chip is made to recieve a 3.3v signal and to trigger the on/off-state of devices using voltages up to 24 V.

This chip has inbuilt surge protection for plugging in solenoid-type devices without causing possible surge currents to the power supply. The trigger-signal pin outlet is also protected against possible power surges coming trough the mosfet component.

 [![tinymosfet](../tinymosfet.png)](../tinymosfet.png)
the Kicad-files for the mosfet-piece can be found [here](tinymosfet.7z)

##     Intefacing and Programming
See work from previous weeks to see the software for the microchips, RPI and UI.
[Week 15 contains the software for Featherm0chips and RPi-I2C-communication](https://karlpalo.gitlab.io/fablab2022/weekly/week15/)
[Week 16 contains the software for UI-development and MYSQL-database management](https://karlpalo.gitlab.io/fablab2022/weekly/week16/)

## System integration and packaging
In this project the focus was wery much on system integration and very little on packaging.

The system had to work and be operable in a lab environment, with the spatial and utlity requirements defined by the space. For example the high-pressure part of the CO2-system (bottles ad regulators) is attached to a wall by a metallic chain and cannot be moved from it's place. Also, the system needed to be set up so, that disassemly and re-setting up could be done relatively fast somewhere nearby if shuffling of the equipment due to other lab equipment would be needed.

The only purpose of packaging was to protect the equpment from misuse, damage and water splashes. Duct tape and ready-made plastic jars were the choice of the day, as these systems are easily moved and replaced if damaged due to interaction with other lab users.


## Project as described in bill of materials:
The projects description and build at June of 2023 can be found [here](https://karlpalo.gitlab.io/fablab2022/mainproject/description/)
The bill of materials can be found [here](https://karlpalo.gitlab.io/fablab2022/mainproject/bom/)
