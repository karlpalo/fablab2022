---
title: Week10 embedded programming
#subtitle: Why you'd want to hang out with me
#comments: false
---

# Individual Assignments

## Read the datasheet for your microcontroller
I read the [datasheet](../ATtiny1614-16-17-DataSheet-DS40002204A.pdf) for attiny 1614,1616 and 1617 (as they are provided in the same document)

In principle, what I understood is that these chips share similar basic (processing speed, memory, etc.) functionalities, but the main difference is in the number of pinouts and number of peripheral devices they can control or be connnected to.

To be honest, the part I found most useful was just the description of pinouts. (section 4 of the manual). As I would use that info continuously in accessing an referring to to the pins in both programming and chip design.

Most of the datasheet, eg. describing the memory structure (section 6), peripherals and architecture (section 7), and the physical functionality and capability of the CPU (section 8) and descriptions of the functionality and operational basis of various sensors and peripherals, like clocks, non volatile memories, I/O-connections etc. (section 19-32) was not useful for me at this point.

However, if I was using this for commercial production, I would need to familiarize myself much better with these properties, to better understand the stability and possible errors and error types present in the chip.

Sections 35 (Electrical Characteristics) & 36 (Typical Characteristics) were of extreme importance! In section 35 the operational range of the chip and its peripherals were clearly defined, and in section 36 data was given to estimate warious properties, such as power consumption and heating up of the chip in use.

Rest of the datasheet was mainly for standardisation and ordering purposes so it provided no real interest to me.


What was useful however, was that the [datasheet for attiny 412](../ 40001911A.pdf) Had the same format and structure as the datasheet for the 1614, thus making large parts of the documents understandable with relatively small amount of extra reading.

What could be interesting would be to read the datasheet of a chip from another family and see if the documentation is as standardized as one could hope!


## Use your programmer to program your board to do something

I did this actually in week 8 already :D As my buttons operated on programmable logic instead of physical connection to the device (LED) it is supposed to power. See the fabpage for week 8 for this

One interesting idea I got quite late would be to use the Rasperry Pi as both serial listener, and an UPDI-programmer, I need to look into this a little bit!


## What I spent my week on

This week I also spent a considerable amount of time studying networking, as I want to set up a Rasperry PI for my final project. My goal is to recieve input from a thermistor connected to attiny 412, and to read it from Rasperry Pi.

To get started, I designed a board with a thermal sensor (the sensor is a thermosensitive resistor) This board used an attiny412 chip (a new chip for me), and the reason for mounting the thermal sensor on board was that Kris suggested that the data should be converted from analog to digital as close as possible to the actual sensor. (thus justifying an extra attiny 412 chip that does only that)


For testing I used the USB-UPDI-programmer made earlier and a Serial-USB-connector made earlier (These used a D11C14A-chip, a beast I might read more about later)

The setup can be seen in the picture below

[![Picture of thermosensor](../thermosensor_connected.jpg)](../thermosensor_connected.jpg)


And the code I uploaded to the attiny412 is below

 ```


 //Testcode for serial connection for thermoboard

// const int Tx = PIN_PA6; // not needed- inbuilt in chip
// const int Rx = PIN_PA7; // not needed- inbuilt in chip
 void setup()

 {
//pinMode(Rx, INPUT);  // not needed- inbuilt in chip
//pinMode(Tx, OUTPUT);  // not needed- inbuilt in chip
 Serial.begin(9600); // send serial data at 9600 bits/sec

 }


 void loop()
 {
   Serial.println("Hello Computer");
   delay(1000);


  }

  ```

Finally below is a video of my computer reciving the data from the attiny412 board.
[![Picture of thermosensor](../thermotest.jpg)](../thermotest.mp4)

## What I had to still do
However, upon further testing, it proved that even though the board was functional, the temperature sensor was unusable. I had not added any reference resistors, thus making it very hard to evaluate the accuracy of the measurement, and any potential errors present caused by the wiring/plating/Space elves. Thus For the design files below, an improved version of my board is added. The improved version also boasts a serial connector for easy connectivity to other chips and computers.
The kicad -work folder of my temperature densor can be found [here](tempsensor_final.7z) as a 7.zip file
