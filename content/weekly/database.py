import sqlite3

import numpy as np
import pandas as pd
from sklearn.metrics import r2_score


#connect = sqlite3.connect("grow_test.db")

#create a cursor


def show_all_tables():
    connect = sqlite3.connect("grow_test.db")
    cur = connect.cursor()
    cur.execute("""SELECT name FROM sqlite_master  
  WHERE type='table';""")
    print(cur.fetchall())
    connect.commit()
    #close our connection
    connect.close()

def show_all_sensordata():
    connect = sqlite3.connect("grow_test.db")
    #create a cursor
    cur = connect.cursor()
    cur.execute('SELECT rowid, * FROM sensordata' )
    items = cur.fetchall()
    for item in items:
         print(item)
    connect.commit()
    #close our connection
    connect.close()

def show_all_commands():
    connect = sqlite3.connect("grow_test.db")
    #create a cursor
    cur = connect.cursor()
    cur.execute('SELECT rowid, * FROM commands' )
    items = cur.fetchall()
    for item in items:
         print(item)
    connect.commit()
    #close our connection
    connect.close()


def show_latest_sensordata():
    connect = sqlite3.connect("grow_test.db")
    cur = connect.cursor()
    cur.execute('SELECT * FROM sensordata ORDER BY rowid DESC LIMIT 1')
    latest= cur.fetchall()
    connect.commit()
    connect.close()
    return latest

def show_latest_commands():
    connect = sqlite3.connect("grow_test.db")
    cur = connect.cursor()
    cur.execute('SELECT * FROM commands ORDER BY rowid DESC LIMIT 1')
    latest= cur.fetchall()
    connect.commit()
    connect.close()
    return latest

def show_latest_phcalibs():
    connect = sqlite3.connect("grow_test.db")
    cur = connect.cursor()
    cur.execute('SELECT * FROM phcalibs ORDER BY rowid DESC LIMIT 1')
    latest= cur.fetchall()
    connect.commit()
    connect.close()
    return latest

def return_latest_commands_item(number):
    connect = sqlite3.connect("grow_test.db")
    cur = connect.cursor()
    cur.execute('SELECT * FROM commands ORDER BY rowid DESC LIMIT 1')
    tuple = cur.fetchall()
    lista = list(tuple[0])
    del lista[0]
    return lista[number]
    connect.commit()
    connect.close()

def return_latest_sensordata_item(number):
    connect = sqlite3.connect("grow_test.db")
    cur = connect.cursor()
    cur.execute('SELECT * FROM sensordata ORDER BY rowid DESC LIMIT 1')
    tuple = cur.fetchall()
    lista = list(tuple[0])
    del lista[0]
    return lista[number]
    connect.commit()
    connect.close()

def return_latest_phcalibs_item(number):
    connect = sqlite3.connect("grow_test.db")
    cur = connect.cursor()
    cur.execute('SELECT * FROM phcalibs ORDER BY rowid DESC LIMIT 1')
    tuple = cur.fetchall()
    lista = list(tuple[0])
    del lista[0]
    return lista[number]
    connect.commit()
    connect.close()


def add_one_sensordata(sensornum,value):
    connect = sqlite3.connect("grow_test.db")
    #create a cursor
    cur = connect.cursor()
    cur.execute('SELECT * FROM sensordata ORDER BY rowid DESC LIMIT 1')
    tuple = cur.fetchall()
    lista = list(tuple[0])
    del lista[0]
    print(lista)
    lista[sensornum] = value
    print(lista)
    cur.execute("INSERT INTO sensordata VALUES (datetime('now'),?,?,?,?,?,?,?,?,?,?,?,?)",(lista))
    connect.commit()
    #close our connection
    connect.close()

def add_all_sensordata(a,b,c,d,e,f,g,h,i,j,k,l):
    connect = sqlite3.connect("grow_test.db")
    cur = connect.cursor()
    cur.execute("INSERT INTO sensordata VALUES (datetime('now'),?,?,?,?,?,?,?,?,?,?,?,?)",(a,b,c,d,e,f,g,h,i,j,k,l))
    connect.commit()
    connect.close()

def add_one_commands(sensornum,value):
    connect = sqlite3.connect("grow_test.db")
    cur = connect.cursor()
    cur.execute('SELECT * FROM commands ORDER BY rowid DESC LIMIT 1')
    tuple = cur.fetchall()
    lista = list(tuple[0])
    del lista[0]
    print(lista)
    lista[sensornum] = value
    print(lista)
    cur.execute("INSERT INTO commands VALUES (datetime('now'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",(lista))
    connect.commit()
    #close our connection
    connect.close()

def add_all_commands(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r):
    connect = sqlite3.connect("grow_test.db")
    cur = connect.cursor()
    cur.execute("INSERT INTO commands VALUES (datetime('now'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r))
    connect.commit()
    connect.close()

def add_one_phcalibs(phslot,value):
    connect = sqlite3.connect("grow_test.db")
    cur = connect.cursor()
    cur.execute('SELECT * FROM phcalibs ORDER BY rowid DESC LIMIT 1')
    tuple = cur.fetchall()
    lista = list(tuple[0])
    del lista[0]
    print(lista)
    lista[phslot] = value
    print(lista)
    cur.execute("INSERT INTO phcalibs VALUES (datetime('now'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",(lista))
    connect.commit()
    #close our connection
    connect.close()

def add_all_phcalibs(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,a1):
    connect = sqlite3.connect("grow_test.db")
    cur = connect.cursor()
    cur.execute("INSERT INTO phcalibs VALUES (datetime('now'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,a1))
    connect.commit()
    connect.close()


def calibrate_sensor_ph4(sensor,value):
    connect = sqlite3.connect("grow_test.db")
    cur = connect.cursor()
    cur.execute('SELECT * FROM phcalibs ORDER BY rowid DESC LIMIT 1')
    tuple = cur.fetchall()
    lista = list(tuple[0])
    del lista[0]
    lista[((sensor-1)*3)] = value
    print(value)
    cur.execute("INSERT INTO phcalibs VALUES (datetime('now'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",(lista))
    connect.commit()
    connect.close()

def calibrate_sensor_ph7(sensor,value):
    connect = sqlite3.connect("grow_test.db")
    cur = connect.cursor()
    cur.execute('SELECT * FROM phcalibs ORDER BY rowid DESC LIMIT 1')
    tuple = cur.fetchall()
    lista = list(tuple[0])
    del lista[0]
    lista[(((sensor-1)*3)+1)] = value
    cur.execute("INSERT INTO phcalibs VALUES (datetime('now'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",(lista))
    connect.commit()
    connect.close()

def calibrate_sensor_ph10 (sensor,value):
    connect = sqlite3.connect("grow_test.db")
    cur = connect.cursor()
    cur.execute('SELECT * FROM phcalibs ORDER BY rowid DESC LIMIT 1')
    tuple = cur.fetchall()
    lista = list(tuple[0])
    del lista[0]
    lista[(((sensor-1)*3)+2)] = value
    cur.execute("INSERT INTO phcalibs VALUES (datetime('now'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",(lista))
    connect.commit()
    connect.close()

def convert_signal_to_pH (sensor,reading):
    connect = sqlite3.connect("grow_test.db")
    cur = connect.cursor()
    cur.execute('SELECT * FROM phcalibs ORDER BY rowid DESC LIMIT 1')
    tuple = cur.fetchall()
    lista = list(tuple[0])
    del lista[0]
    connect.commit()
    connect.close()
    if sensor == 1:
        x = [lista[0],lista[1],lista[2]]
    if sensor == 2:
        x = [lista[3],lista[4],lista[5]]
    if sensor == 3:
        x = [lista[6],lista[7],lista[8]]
    if sensor == 4:
        x = [lista[9],lista[10],lista[11]]
    if sensor == 5:
        x = [lista[12],lista[13],lista[14]]
    if sensor == 6:
        x = [lista[15],lista[16],lista[17]]
    if sensor == 7:
        x = [lista[18],lista[19],lista[20]]
    if sensor == 8:
        x = [lista[21],lista[22],lista[23]]
    if sensor == 9:
        x = [lista[24],lista[25],lista[26]]
    y = [4,7,10]
    counter = 0
    for i in x:
        if i == None:
            x[counter] = 1023
        counter = counter +1
    x = np.asarray(x, dtype='float64')
    y = np.asarray(y, dtype='float64')
    if reading == None:
        reading = 0
    if reading == ' ':
        reading = 0
    model = np.polyfit(x,y,1)
    predict = np.poly1d(model)

    ph = (predict(reading))
    r2 = (r2_score(y,predict(x)))
    ph = round(ph,1)
    if r2 > 0.5:
        return ph
    else:
        return 15

def convert_pH_to_signal (sensor,pH):
    connect = sqlite3.connect("grow_test.db")
    cur = connect.cursor()
    cur.execute('SELECT * FROM phcalibs ORDER BY rowid DESC LIMIT 1')
    tuple = cur.fetchall()
    lista = list(tuple[0])
    del lista[0]
    connect.commit()
    connect.close()
    if sensor == 1:
        x = [lista[0],lista[1],lista[2]]
    if sensor == 2:
        x = [lista[3],lista[4],lista[5]]
    if sensor == 3:
        x = [lista[6],lista[7],lista[8]]
    if sensor == 4:
        x = [lista[9],lista[10],lista[11]]
    if sensor == 5:
        x = [lista[12],lista[13],lista[14]]
    if sensor == 6:
        x = [lista[15],lista[16],lista[17]]
    if sensor == 7:
        x = [lista[18],lista[19],lista[20]]
    if sensor == 8:
        x = [lista[21],lista[22],lista[23]]
    if sensor == 9:
        x = [lista[24],lista[25],lista[26]]
    y = [4,7,10]        
    model = np.polyfit(x,y,1)
    sensorvalue = model[0]*pH
    round(sensorvalue,2)
    return sensorvalue

def updatevalvestate():
    sensordata = show_latest_sensordata()
    sensordatalist = list(sensordata[0])
    del sensordatalist[0]
    #print(sensordatalist)

    currph1 = convert_signal_to_pH(1,sensordatalist[0])
    targetph1 = return_latest_commands_item(0)
    if currph1 > targetph1:
        add_one_commands(9,"open")
    else:
        add_one_commands(9,"closed")

    currph2 = convert_signal_to_pH(2,sensordatalist[1])
    targetph2 = return_latest_commands_item(1)
    if currph2 > targetph2:
        add_one_commands(10,"open")
    else:
        add_one_commands(10,"closed")
    
    currph3 = convert_signal_to_pH(3,sensordatalist[2])
    targetph3 = return_latest_commands_item(2)
    if currph3 > targetph3:
        add_one_commands(11,"open")
    else:
        add_one_commands(11,"closed")
    
    currph4 = convert_signal_to_pH(4,sensordatalist[3])
    targetph4 = return_latest_commands_item(3)
    if currph4 > targetph4:
        add_one_commands(12,"open")
    else:
        add_one_commands(12,"closed")
    
    currph5 = convert_signal_to_pH(5,sensordatalist[4])
    targetph5 = return_latest_commands_item(4)
    if currph5 > targetph5:
        add_one_commands(13,"open")
    else:
        add_one_commands(13,"closed")
    
    currph6 = convert_signal_to_pH(6,sensordatalist[5])
    targetph6 = return_latest_commands_item(5)
    if currph6 > targetph6:
        add_one_commands(14,"open")
    else:
        add_one_commands(14,"closed")
    
    currph7 = convert_signal_to_pH(7,sensordatalist[6])
    targetph7 = return_latest_commands_item(6)
    if currph7 > targetph7:
        add_one_commands(15,"open")
    else:
        add_one_commands(15,"closed")
    
    currph8 = convert_signal_to_pH(8,sensordatalist[7])
    targetph8 = return_latest_commands_item(7)
    if currph8 > targetph8:
        add_one_commands(16,"open")
    else:
        add_one_commands(16,"closed")
    
    currph9 = convert_signal_to_pH(9,sensordatalist[8])
    targetph9 = return_latest_commands_item(8)
    if currph9 > targetph9:
        add_one_commands(17,"open")
    else:
        add_one_commands(17,"closed")

#lookup with where
#read latest row
#"SELECT * FROM TableName ORDER BY rowid DESC LIMIT 1;"

#SQLite datatypes
# NULL, (null if does not exist, not null if exists)
# INTEGER (number)
# REAL (decimal)
# TEXT (text)
# BLOB (stored as is, images,music,blob of data)

#create a table
# tables commands_test and sensordata_test
#connect = sqlite3.connect("grow_test.db")
    #create a cursor
#cur = connect.cursor()
#cur.execute("""CREATE TABLE sensordata(
# timestamp TEXT,
# phsensor1 INTEGER,
# phsensor2 INTEGER,
# phsensor3 INTEGER,
# phsensor4 INTEGER,
# phsensor5 INTEGER,
# phsensor6 INTEGER,
# phsensor7 INTEGER,
# phsensor8 INTEGER,
# phsensor9 INTEGER,
# tempsensor1 REAL,
# tempsensor2 REAL,
# tempsensor3 REAL
# )
#""")

#cur.execute("""DROP TABLE sensordata_test""")

#cur.execute("""CREATE TABLE commands (
# timestamp   TEXT,
# ph1setvalue REAL,
# ph2setvalue REAL,
# ph3setvalue REAL,
# ph4setvalue REAL,
# ph5setvalue REAL,
# ph6setvalue REAL,
# ph7setvalue REAL,
# ph8setvalue REAL,
# ph9setvalue REAL,
# valve1force TEXT,
# valve2force TEXT,
# valve3force TEXT,
# valve4force TEXT,
# valve5force TEXT,
# valve6force TEXT,
# valve7force TEXT,
# valve8force TEXT,
# valve9force TEXT
#  )
#"""
#            )

#connect.commit()
    #close our connection
#connect.close()

#cur.execute("INSERT INTO commands_test VALUES('7a','8as','3','3')")

#manysensors = [
#    ('3','2','3','4','5','6','7','8','91'),
#    ('3','2','3','4','5','6','7','8','92'),
#    ('3','2','3','4','5','6','7','8','93'),]

#cur.executemany("INSERT INTO devicedata VALUES (?,?,?,?,?,?,?,?,?)",manysensors)
#cur.execute("SELECT * FROM devicedata WHERE phsensor9 >= 91 ") #LIKE "AB%"

#cur.execute("SELECT rowid,* FROM devicedata")


# update records
#cur.execute("""UPDATE devicedata SET phsensor2 = 55
#                WHERE phsensor9 = 91 """)
#cur.execute("""UPDATE devicedata SET phsensor2 = 55
#                WHERE rowid = 1 """)

#delete record
#cur.execute("DELETE from devicedata WHERE rowid = 6")

#order record
# cur.execute('SELECT rowid, * FROM devicedata  ORDER BY rowid DESC' ) #or ASC
#AND/OR
#cur.execute('SELECT rowid, * FROM devicedata WHERE phsensor1 > 1 AND phsensor9 > 7' ) #replace and with OG

#Limiting results
#cur.execute('SELECT rowid, * FROM devicedata LIMIT 3' )

# droptable


#connect = sqlite3.connect("grow_test.db")
#cur = connect.cursor()
#cur.execute("DROP TABLE commands_test")
#connect.commit()
    #close our connection
#connect.close()



#cur.execute('SELECT rowid, * FROM devicedata LIMIT 3' )


#items = cur.fetchall()

#for item in items:
 #   print(item)



#print(cur.fetchone()[0]) #gets first value of first item
#cur.fetchmany(3)
#print(cur.fetchall()[4]) #gets full tuple item 5
#items = cur.fetchall()
#for item in items:
#    print(str(item[0]) + " " + str(item[1]))
#print(items)

