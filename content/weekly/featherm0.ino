/* 
 * Arduino code to send and receive I2C data
 * Tested on Adafruit Feather M0+ Express and Raspberry Pi Model 4
 * 
 * SDA <--> SDA
 * SCL <--> SCL
 * GND <--> GND
 * 
 * Sets built-in LED (1 = on, 0 = off) on Feather when requested 
 * and responds with data received
 */
#include <Wire.h>
#define SLAVE_ADDRESS 0x10       // I2C address for Arduino
#define LED 13                   // Built-in LED
#define refhigh 5               //reference voltage for rpi

 // START OF TEMPSENSOR CODE
#include <OneWire.h>
#include <DallasTemperature.h>
// Data wire is connected to the Arduino analog pin MISO
#define ONE_WIRE_BUS MISO
#define PHSENSOR1 A3
#define PHSENSOR2 A2
#define PHSENSOR3 A1
#define PHSENSOR4 A0
#define VALVE1 13
#define VALVE2 12
#define VALVE3 11
#define VALVE4 10

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire1(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature sensor 
DallasTemperature sensort1(&oneWire1);



int i2cData = 0;                 // the I2C data received
byte ph1low;
byte ph1high;
int testint = 1234;
int number = 0;
int num1 = 0;
int num2 = 0;
int num3 = 0;
int num4 = 0;
int t1 = 0;
int ph1 = 0;
int ph2 = 0;
int ph3 = 0;
int ph4 = 0;

void setup(){
  Wire.begin(SLAVE_ADDRESS);
  Wire.onReceive(receiveData);
  Wire.onRequest(sendaData);
  pinMode(LED, OUTPUT);
  digitalWrite(LED, HIGH);
  pinMode(refhigh, OUTPUT);
  digitalWrite(refhigh,HIGH);
}
void loop() {

  sensort1.requestTemperatures();
  t1 = sensort1.getTempCByIndex(0);
  
  ph1 = analogRead(PHSENSOR1);
  ph2 = analogRead(PHSENSOR2);
  ph3 =analogRead(PHSENSOR3);
  ph4 =analogRead(PHSENSOR4);


  
}
// Handle reception of incoming I2C data
void receiveData(int byteCount) {
  while (Wire.available()) {
    i2cData = Wire.read();
    if (i2cData == 1) {
      digitalWrite(LED, 1);
    }
    else {
      digitalWrite(LED, 0);
    }
  }
}
// Handle request to send I2C data
void sendaData() { 
inttobinary(1234);
inttobinary(t1);
inttobinary(ph1);
inttobinary(ph2);
inttobinary(ph3);
inttobinary(ph4);
inttobinary(4321);
}

void inttobinary(int number){
  num4 = (number) % 10; //rightmost number
  num3 = (number / 10) % 10;
  num2 = (number / 100) % 10;
  num1 = (number / 1000);
  Wire.write(num1);
  Wire.write(num2);
  Wire.write(num3);
  Wire.write(num4);

}
