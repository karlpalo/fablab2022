---
title: Week 16 Interface and Application Programming
#subtitle: Why you'd want to hang out with me
#comments: false
---
##Assingment outline
Follow the steps below to complete your assignment.

    -Write an application that interfaces a user with an input and/or output device that you made.
    -Include a hero video and source files of the application in your documentation.
    -Describe what you learned about application and interface building on your documentation website.
    -Submit a link to your assignment page here.

## Demonstration of an UI built with Dash
[Dash](https://plotly.com/dash/) is an commercial framework for building data science apps. Their enterprise branch offers a web UI you can use to build your own data apps. However, most of the functionality is also present in the Open Source python package they develop. [The support and manuals for this open source package is also found on their website](https://dash.plotly.com/?_gl=1*okxi4n*_ga*NzQxMjQ5ODAxLjE2ODU1NDkxNTY.*_ga_6G7EE0JNSC*MTY4NTU0OTE1NS4xLjEuMTY4NTU0OTE2My4wLjAuMA..) Using this open source package is more laborious, but also more educative, as no automatic bells and whistles are added. (However, as UI-development goes, this is my first real UI, and the code and formatting are not ideal...)

### How it looks like, what it does

  * Sidebar functionality
  [![sidebardemo](../sidebar.png)](../sidebar.mp4)
  * Reading of sensor data
    [![sidebardemo](../dummyph.png)](../dummyph.mp4)
  * Calibrating pH-meters
  [![calibrationdemo](../calibration.png)](../calibration.mp4)

## What did I learn  
This was actually one of the most challenging weeks for me. In addition to interactive UI creation I had to really think about the data management and how the system would function if something like a power failure occurred.

Learning DASH for the UI generation was a long challenge, especially as the open-source version has some hijinks in the documentation (which are according to forums fixed or better documented in the commercial version)
I had to do plenty of debugging and test versions to end up with an UI that functioned as I wanted it to (e.g. updates the sensor data, valve statuses, pH-calibrations and temperature readings each second from the database) and looked the way I wanted it to look. To get everything working in harmony took several months. However, when everything finally worked, satisfaction was immense.

As an example of the encountered difficulties: In Dash, it is possible to use functions to create the structure of the website. (e.g. to create 3 boxes, you could run a self-made createbox()-function 3 times) Unfortunately, assigning handles to these function created elements is not at the moment possible (I discovered this after much debugging pain, in the DASH-github issue tracker.....). Thus to create 9 cells for pH-sensor reading and calibration, much manual copypasteing and editining of labels was done. This made the code very unastethic, but functional and easy to debug.

Also the SQL-database usage is a bit like killing a mouse with a nuclear bomb. A CSV-file that would be updated regularly could have been fine, or internal data storages in the program.
However using an SQL-database makes this solution (almost) infinitely scalable and very foolproof in regards to unexpected user input. Another reason to go with SQL was my discussion with a professional programmer friend, who said that SQL-tables are especially good in the sense that it is not so easy to lose data when adding a new line (and something unexpected like data corruption or loss of power happens). And the data will in most cases be easily recoverable. Also, internal shortcuts to timestamping were nice.

Of course a tech-savy user can fool around with DROP ALL TABLES - command, so most likely I will not add wifi access to the SQL-tables to avoid any "suprises" by curious georges.



## FIles needed
- [UI.py](UI.py)
- [Database.py](../database.py) (For the SQLite management)
