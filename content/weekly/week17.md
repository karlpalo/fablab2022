---
title: Week 17 Wildcard week
#subtitle: Why you'd want to hang out with me
#comments: false
---

## Self made lab beaker? (and lab glassware in the future?)

I decided to test my skills in glass-making and recreate a lab-glass beaker

I would also plan to ask the glass workshop masters if using clean glass-waste from labs (broken glassware) in glass manufacturing would be fine. Lab glass is called PYREX-glass and is chemically more inert than usual varieties of glass. However, as I am just melting it, it should work fine I hope?

Below is the design of my beaker, adhering to the given design rules (6mm wall thickness, 12mm hole at the "fill point") The volume of the beaker was calculated in freeCad to be 278cm^3.
[![Beakervolume ](../beakervolume.png)](../beakervolume.png)

The final design has a funnel with a volume of ~300 cm^3 to ensure that there is enough space to pack the glass for the mold
[![Beakervolume ](../beakervolume2.png)](../beakervolume2.png)

The Cad-file and stl file can be found below.
[Freecad-file](../glasscup.FCStd)
[stl-file](../glasscup.stl)

## Assembly of the 3D-printed Model
The requirement of the glass workshop was that the model had to be printed entirely in natural (no additives allowed) PLA.
To speed up the printing (and to ensure the model fit in the 3D-printer) the model was printed in two pieces
[![Beakerprint ](../beakerprint.jpg)](../beakerprint.jpg)

Both of the pieces had 1/2 of the 12mm connection channel in them. To reassamble the model, I used a heat gun set at 240 C (melting point of PLA is ~220)
to melt the plastic at both ends of the connection channel.
[![Beakermelt ](../beakermelt.jpg)](../beakermelt.jpg)


Finally I assembled the model by pressing the melted ends gently together and holding the top part of the model in place until PLA had cooled and solidified.
This left me with a model ready to wait casting of the negative mold and glass in the upcoming autumn.

[![Beakermelt ](../beakerassem.jpg)](../beakerassem.jpg)

## TBD in autumn 2023 (CASTING THE GLASS MOLD AND GLASS)
