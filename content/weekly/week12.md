---
title: Week12 Output devices

#subtitle: Why you'd want to hang out with me
#comments: false
---
## Desigining the output devices
For outuput devices week, I decided to do a redesing on a DC-motordriver chip made by Krisjanis Rijnieks. [The github for the chip can be found here.](https://gitlab.com/kriwkrow/dc-motor-driver/-/tree/main/)

What I wanted to do is to convert the purely UPDI-connected motor driver to something that has a TX/RX-connection. I also wanted the chip to have two power sources instead of single 12V one, e.g. the chip would be powered by the same device that it has TX/RX connection to.

The Krisjanis design and mine can be seen side by side below.

[![Board design](../dcboard.jpg)](../dcboard.jpg)

Here are the differences in design
- Mounting pins (For chip stability)
- Mounting pins with TX/RX/VCC/GND-connections
- 2 Jumper resistors
- Copper fill as GND
- rotating and rewiring the components for effective use of space and to account for new connectivity and functionality

## Testing the output devices

The output device was tested with a water pump using the original DC-motor testing code from Krisjanis Github. (The Pins for output were change due to some rewiring of the chip) This code is supposed to spin the motor in two directions with a varying speed.

```
/*
 * Name: dc-motor-driver test.ino
 *
 * Description: H-bridge DC motor PWM speed and direction control
 *
 * Author: Krisjanis Rijnieks
 * Last modified: 23 Jun 2021
 *
 *
 *
 * MIT License
 *
 * Copyright (c) 2021 Krisjanis Rijnieks
 *
 * Code modified by Karl Mihhels on 25 Apr 2022, connection pins of MOT_A1 and MOT_A2 changed
 *
 */

#define MOT_A1 3
#define MOT_A2 4

void setup() {
  // Nothing tp do here.
}

void loop() {
  fwd();
  delay(1000);
  rev();
  delay(1000);
}

void fwd() {
  analogWrite(MOT_A2, LOW);

  for (int i = 0; i < 256; i++) {
    analogWrite(MOT_A1, i);
    delay(2);
  }

  delay(2000);

  for (int i = 255; i > -1; i--) {
    analogWrite(MOT_A1, i);
    delay(2);
  }
}

void rev() {
  analogWrite(MOT_A1, LOW);

  for (int i = 0; i < 256; i++) {
    analogWrite(MOT_A2, i);
    delay(1);
  }

  delay(2000);

  for (int i = 255; i > -1; i--) {
    analogWrite(MOT_A2, i);
    delay(1);
  }
}
```


In the video below you can see the motor working, however, it only works half of the time. There is no visual of the motor running as the spinning part is hidden inside the chassis, but when listening to the video you can hear the motor turning off and on clearly. (In the live setting also the vibration of the motor was observable)
[![Motor run observation](../spinningmotor.jpg)](../spinningmotor.webm)

 After some debugging (I measured the output voltage of the chip from the wires connected to the motors) I found out that the problem was not with the chip but with the motor. It was built to recieve current only in one direction (an unusual design for a DC-motor). Most likely the reason is that the motor was specified for 6-12 V, and contains most likely some voltage regulators as internal system.

From a video taken right after the previous one, one can see the power supply voltage and the current it is feeding to spin the rotor. One can see that there is a peak current as the motor starts and the current stabilizes as the motor runs.
[![Voltage observation](../voltage.jpg)](../voltage.webm)

From knowing the voltage and the current the power consumption of the motor can be calcultaed as: P = V·I
The Voltage was 12,3 V, and the maximum current was 0,13 A and the running current was 0,07 A

Thus the power consumption for startup was ~1,6 W and for running ~0,85 W
