import datetime
import dash
import dash_bootstrap_components as dbc
import dash_daq as daq
from PIL import Image
import database

app = dash.Dash(external_stylesheets=[dbc.themes.DARKLY])

from dash import Input, Output, State, html, dcc
from dash_bootstrap_components._components.Container import Container


ph1_store = dcc.Store(id="ph1-store", storage_type="local")



# LINKS TO LOGOS AND IMAGE FILES
#Used in navbar
ALGALY_LOGO = "https://cdn3.iconfinder.com/data/icons/biofuel-green-energy-1/100/algae_cultivation_bio_fuel_biofuel_green_railway-512.png"

tab_style = {
    'border': '1px solid #FFFFFF',
    'backgroundColor': '#111111',
}

tab_selected_style = {
    'border': '1px solid #FFFFFF',
    'borderLeft': '3px solid #f39c12',
    'backgroundColor': '#375a7f',
    'color': 'white',
}


sidebar_navigation = dcc.Tabs( id="sidebar-tablist", vertical=True, value='tab-mainscreen', children=[
        dcc.Tab(label='Main screen', value='tab-mainscreen',style=tab_style, selected_style=tab_selected_style),
       # dcc.Tab(label='Configuration', value='tab-config',style=tab_style, selected_style=tab_selected_style),
        dcc.Tab(label='Controldiagram', value='tab-controldiagram',style=tab_style, selected_style=tab_selected_style),
        dcc.Tab(label='Programs', value='tab-programs',style=tab_style, selected_style=tab_selected_style),
       # dcc.Tab(label='Data import/export', value='tab-dataimportexport',style=tab_style, selected_style=tab_selected_style),
       # dcc.Tab(label='Data analysis', value='tab-dataanalysis',style=tab_style, selected_style=tab_selected_style),

    ]
    )

tab_mainscreen_content = html.Div([dbc.Row(html.H3("AlgaeGrower-project")),
dbc.Row([dbc.Col("AlgaeGrower is a still developing device, aimed for simulating different cultivation conditions for algae. Albeit aquariums do exist, advanced systmes with simultaneous control parameters such as light intensity, water movement, pH, nutrient feeds do not exist."
                 "Furthermore, this system includes process automation capabilities, allowing for automatic adjustment of cultivaton conditions")],),
dbc.Row(html.H3("Features present in exhibition model")),
dbc.Row("- Cool UI using python and Dash (This webpage is generated in python)"),
dbc.Row("- Programmable functionality (Exhibition setup runs on a 60 min loop)"),
dbc.Row("- Light intensity control (To simulate full sunlight/overcast/night)"),
dbc.Row("- Water movement control (To simulate still/stormy seas)"),
dbc.Row("- Temperature sensor (The first of many sensors to come)"),
dbc.Row("- Individual device control (Disabled for the exhibition)"),

dbc.Row(html.H3("Features still in development")),
dbc.Row([dbc.Row("- pH-sensing via a dedicated sensor"),
    dbc.Row("- pH/carbon supply-control via CO2-feed"),
    dbc.Row("- Nutrient pump system"),
    dbc.Row("- Camera and live visual data collection")]),
])

tab_config_content = html.Div([
    dbc.Row(html.H3('Tab content 2'),),
])



ph1_input = html.Div(
    [
       # dbc.Label(""),

    ]
)

phsetpointlist = ["ph1setpoint","ph2setpoint","ph3setpoint","ph4setpoint","ph5setpoint","ph6setpoint","ph7setpoint","ph8setpoint","ph9setpoint"]
phbuttonlist = ["ph1button","ph2button","ph3button","ph4button","ph5button","ph6button","ph7button","ph8button","ph9button"]
phinputlist= ["ph1-input","ph2-input","ph3-input","ph4-input","ph5-input","ph6-input","ph7-input","ph8-input","ph9-input"]
"""
for index, item in enumerate(phsetpointlist):
    print(item,index)
    @app.callback(
         Output(phsetpointlist[index], 'children'),
         Input(phbuttonlist[index], 'n_clicks'),
         State(phinputlist[index],'value')
        )
    def on_button_click(n_clicks,value):
         if n_clicks == 0:
               return "nan"
         database.add_one_commands(index,value)
         return value
"""


ph1screen = dbc.Col(
    [dbc.Row(html.Div("pH-sensor 1"),),
        dbc.Row(html.Div(id="ph1live"),),
        dcc.Interval(id='interval-component',interval=1000, n_intervals=0),
        dbc.Row(html.Div("Valve open if pH > than"),),
        dbc.Row(html.Div(id="ph1setpoint"),),
        dbc.Row(html.Div([dcc.Input(id="ph1-input", type="number", value="", min=0, max=14, step=0.1),
        dbc.FormText("Valve setpoint (0-14, 0.1 step)"),]),),
        dbc.Row(html.Div(dbc.Button("Update setpoint", color="primary", className="me-1", id="ph1button", n_clicks=0),),),
        dbc.Row(html.Div(dbc.Button("open calibration", color="primary", className="me-1", id="ph1calibbutton", n_clicks=0),),),
        dbc.Modal(
            [
                dbc.ModalHeader("PH-calibration"),
                dbc.ModalBody(dbc.Col([
                                  dbc.Row([html.Div("current (sensor 1) reading"),html.Div(id="ph1status"),]),
                                  dbc.Row(html.Div(dbc.Button("Update sensor reading", color="primary", className="me-1", id="ph1-sens-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph4"),html.Div(id="ph1_4"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph1-ph4input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph4", color="primary", className="me-1", id="ph1-ph4-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph7"),html.Div(id="ph1_7"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph1-ph7input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph7", color="primary", className="me-1", id="ph1-ph7-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph10"),html.Div(id="ph1_10"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph1-ph10input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph10", color="primary", className="me-1", id="ph1-ph10-button", n_clicks=0),),),

                     ]),),
                dbc.ModalFooter(
                    dbc.Button("CLOSE BUTTON", id="close1", className="ml-auto")
                ),
            ],
            id="modal1",
        ),
        ],)
@app.callback([Output('ph1live', 'children'),
               Output('ph2live', 'children'),
               Output('ph3live', 'children'),
               Output('ph4live', 'children'),
               Output('ph5live', 'children'),
               Output('ph6live', 'children'),
               Output('ph7live', 'children'),
               Output('ph8live', 'children'),
               Output('ph9live', 'children'),],
              Input('interval-component', 'n_intervals'))
def update_metrics(n):
    ph1status = database.return_latest_sensordata_item(0)
    ph1status = database.convert_signal_to_pH(1,ph1status)
    ph2status = database.return_latest_sensordata_item(1)
    ph2status = database.convert_signal_to_pH(2,ph2status)
    ph3status = database.return_latest_sensordata_item(2)
    ph3status = database.convert_signal_to_pH(3,ph3status)
    ph4status = database.return_latest_sensordata_item(3)
    ph4status = database.convert_signal_to_pH(4,ph4status)
    ph5status = database.return_latest_sensordata_item(4)
    ph5status = database.convert_signal_to_pH(5,ph5status)
    ph6status = database.return_latest_sensordata_item(5)
    ph6status = database.convert_signal_to_pH(6,ph6status)
    ph7status = database.return_latest_sensordata_item(6)
    ph7status = database.convert_signal_to_pH(7,ph7status)
    ph8status = database.return_latest_sensordata_item(7)
    ph8status = database.convert_signal_to_pH(8,ph8status)
    ph9status = database.return_latest_sensordata_item(8)
    ph9status = database.convert_signal_to_pH(9,ph9status)
    return ph1status,ph2status,ph3status,ph4status,ph5status,ph6status,ph7status,ph8status,ph9status

@app.callback([Output('ph1status', 'children',allow_duplicate=True),
              Output('ph1_4', 'children',allow_duplicate=True),
              Output('ph1_7', 'children',allow_duplicate=True),
              Output('ph1_10', 'children',allow_duplicate=True),],[
              Input("ph1-sens-button", 'n_clicks')],
              prevent_initial_call=True)
def checkcalib1(n_clicks):
    ph1status = database.return_latest_sensordata_item(0)
    ph1_4 = database.return_latest_phcalibs_item(0)
    ph1_7 = database.return_latest_phcalibs_item(1)
    ph1_10 = database.return_latest_phcalibs_item(2)
    return ph1status,ph1_4,ph1_7,ph1_10

@app.callback([Output('ph1status', 'children',allow_duplicate=True),
              Output('ph1_4', 'children',allow_duplicate=True),
              Output('ph1_7', 'children',allow_duplicate=True),
              Output('ph1_10', 'children',allow_duplicate=True),],[
              Input("ph1-ph4-button", 'n_clicks')],
              State('ph1-ph4input','value'),
              prevent_initial_call=True)
def calib1ph4(n_clicks,value):
    database.calibrate_sensor_ph4(1,value)
    ph1status = database.return_latest_sensordata_item(0)
    ph1_4 = database.return_latest_phcalibs_item(0)
    ph1_7 = database.return_latest_phcalibs_item(1)
    ph1_10 = database.return_latest_phcalibs_item(2)
    return ph1status,ph1_4,ph1_7,ph1_10

@app.callback([Output('ph1status', 'children',allow_duplicate=True),
              Output('ph1_4', 'children',allow_duplicate=True),
              Output('ph1_7', 'children',allow_duplicate=True),
              Output('ph1_10', 'children',allow_duplicate=True),],[
              Input("ph1-ph7-button", 'n_clicks')],
              State('ph1-ph7input','value'),
              prevent_initial_call=True)
def calib1ph7(n_clicks,value):
    database.calibrate_sensor_ph7(1,value)
    ph1status = database.return_latest_sensordata_item(0)
    ph1_4 = database.return_latest_phcalibs_item(0)
    ph1_7 = database.return_latest_phcalibs_item(1)
    ph1_10 = database.return_latest_phcalibs_item(2)
    return ph1status,ph1_4,ph1_7,ph1_10

@app.callback([Output('ph1status', 'children',allow_duplicate=True),
              Output('ph1_4', 'children',allow_duplicate=True),
              Output('ph1_7', 'children',allow_duplicate=True),
              Output('ph1_10', 'children',allow_duplicate=True),],[
              Input("ph1-ph10-button", 'n_clicks')],
              State('ph1-ph10input','value'),
              prevent_initial_call=True)
def calib1ph10(n_clicks,value):
    database.calibrate_sensor_ph10(1,value)
    ph1status = database.return_latest_sensordata_item(0)
    ph1_4 = database.return_latest_phcalibs_item(0)
    ph1_7 = database.return_latest_phcalibs_item(1)
    ph1_10 = database.return_latest_phcalibs_item(2)
    return ph1status,ph1_4,ph1_7,ph1_10




@app.callback(
         Output('ph1setpoint', 'children'),
         Input('ph1button', 'n_clicks'),
         State('ph1-input','value')
        )
def on_buttonph1_click(n_clicks,value):
    if n_clicks == 0:
        latestvalue = database.return_latest_commands_item(0)
        return latestvalue
    database.add_one_commands(0,value)
    return value


@app.callback(
    Output("modal1", "is_open"),
    [Input("ph1calibbutton", "n_clicks"), Input("close1", "n_clicks")],
    [State("modal1", "is_open")],
)
def toggle_modalph1(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open


ph2screen = dbc.Col(
    [dbc.Row(html.Div("pH-sensor 2"),),
        dbc.Row(html.Div(id="ph2live"),),
        dbc.Row(html.Div("Valve open if pH > than"),),
        dbc.Row(html.Div(id="ph2setpoint"),),
        dbc.Row(html.Div([dcc.Input(id="ph2-input", type="number", value="", min=0, max=14, step=0.1),
        dbc.FormText("Valve setpoint (0-14, 0.1 step)"),]),),
        dbc.Row(html.Div(dbc.Button("Update setpoint", color="primary", className="me-1", id="ph2button", n_clicks=0),),),
        dbc.Row(html.Div(dbc.Button("open calibration", color="primary", className="me-1", id="ph2calibbutton", n_clicks=0),),),
dbc.Modal(
            [
                dbc.ModalHeader("PH-calibration"),
                dbc.ModalBody(dbc.Col([
                                  dbc.Row([html.Div("current (sensor 2) reading"),html.Div(id="ph2status"),]),
                                  dbc.Row(html.Div(dbc.Button("Update sensor reading", color="primary", className="me-1", id="ph2-sens-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph4"),html.Div(id="ph2_4"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph2-ph4input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph4", color="primary", className="me-1", id="ph2-ph4-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph7"),html.Div(id="ph2_7"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph2-ph7input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph7", color="primary", className="me-1", id="ph2-ph7-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph10"),html.Div(id="ph2_10"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph2-ph10input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph10", color="primary", className="me-1", id="ph2-ph10-button", n_clicks=0),),),

                     ]),),
                dbc.ModalFooter(
                    dbc.Button("CLOSE BUTTON", id="close2", className="ml-auto")
                ),
            ],
            id="modal2",
        ),
        ],)

@app.callback([Output('ph2status', 'children',allow_duplicate=True),
              Output('ph2_4', 'children',allow_duplicate=True),
              Output('ph2_7', 'children',allow_duplicate=True),
              Output('ph2_10', 'children',allow_duplicate=True),],[
              Input("ph2-sens-button", 'n_clicks')],
              prevent_initial_call=True)
def calib2(n_clicks):
    ph2status = database.return_latest_sensordata_item(1)
    ph2_4 = database.return_latest_phcalibs_item(3)
    ph2_7 = database.return_latest_phcalibs_item(4)
    ph2_10 = database.return_latest_phcalibs_item(5)
    return ph2status,ph2_4,ph2_7,ph2_10

@app.callback([Output('ph2status', 'children',allow_duplicate=True),
              Output('ph2_4', 'children',allow_duplicate=True),
              Output('ph2_7', 'children',allow_duplicate=True),
              Output('ph2_10', 'children',allow_duplicate=True),],[
              Input("ph2-ph4-button", 'n_clicks')],
              State('ph2-ph4input','value'),
              prevent_initial_call=True)
def calib2ph4(n_clicks,value):
    database.calibrate_sensor_ph4(2,value)
    ph2status = database.return_latest_sensordata_item(1)
    ph2_4 = database.return_latest_phcalibs_item(3)
    ph2_7 = database.return_latest_phcalibs_item(4)
    ph2_10 = database.return_latest_phcalibs_item(5)
    return ph2status,ph2_4,ph2_7,ph2_10

@app.callback([Output('ph2status', 'children',allow_duplicate=True),
              Output('ph2_4', 'children',allow_duplicate=True),
              Output('ph2_7', 'children',allow_duplicate=True),
              Output('ph2_10', 'children',allow_duplicate=True),],[
              Input("ph2-ph7-button", 'n_clicks')],
              State('ph2-ph7input','value'),
              prevent_initial_call=True)
def calib2ph7(n_clicks,value):
    database.calibrate_sensor_ph7(2,value)
    ph2status = database.return_latest_sensordata_item(1)
    ph2_4 = database.return_latest_phcalibs_item(3)
    ph2_7 = database.return_latest_phcalibs_item(4)
    ph2_10 = database.return_latest_phcalibs_item(5)
    return ph2status,ph2_4,ph2_7,ph2_10

@app.callback([Output('ph2status', 'children',allow_duplicate=True),
              Output('ph2_4', 'children',allow_duplicate=True),
              Output('ph2_7', 'children',allow_duplicate=True),
              Output('ph2_10', 'children',allow_duplicate=True),],[
              Input("ph2-ph10-button", 'n_clicks')],
              State('ph2-ph10input','value'),
              prevent_initial_call=True)
def calib2ph10(n_clicks,value):
    database.calibrate_sensor_ph10(2,value)
    ph2status = database.return_latest_sensordata_item(1)
    ph2_4 = database.return_latest_phcalibs_item(3)
    ph2_7 = database.return_latest_phcalibs_item(4)
    ph2_10 = database.return_latest_phcalibs_item(5)
    return ph2status,ph2_4,ph2_7,ph2_10

@app.callback(
    Output("modal2", "is_open"),
    [Input("ph2calibbutton", "n_clicks"), Input("close2", "n_clicks")],
    [State("modal2", "is_open")],
)
def toggle_modal2(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open


@app.callback(
         Output('ph2setpoint', 'children'),
         Input('ph2button', 'n_clicks'),
         State('ph2-input','value')
        )
def on_button_clickph2(n_clicks,value):
    if n_clicks == 0:
        latestvalue = database.return_latest_commands_item(1)
        return latestvalue
    database.add_one_commands(1,value)
    return value


ph3screen = dbc.Col(
    [dbc.Row(html.Div("pH-sensor 3"),),
        dbc.Row(html.Div(id="ph3live"),),
        dbc.Row(html.Div("Valve open if pH > than"),),
        dbc.Row(html.Div(id="ph3setpoint"),),
        dbc.Row(html.Div([dcc.Input(id="ph3-input", type="number", value="", min=0, max=14, step=0.1),
        dbc.FormText("Valve setpoint (0-14, 0.1 step)"),]),),
        dbc.Row(html.Div(dbc.Button("Update setpoint", color="primary", className="me-1", id="ph3button", n_clicks=0),),),
        dbc.Row(html.Div(dbc.Button("open calibration", color="primary", className="me-1", id="ph3calibbutton", n_clicks=0),),),
dbc.Modal(
            [
                dbc.ModalHeader("PH-calibration"),
                dbc.ModalBody(dbc.Col([
                                  dbc.Row([html.Div("current (sensor 3) reading"),html.Div(id="ph3status"),]),
                                  dbc.Row(html.Div(dbc.Button("Update sensor reading", color="primary", className="me-1", id="ph3-sens-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph4"),html.Div(id="ph3_4"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph3-ph4input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph4", color="primary", className="me-1", id="ph3-ph4-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph7"),html.Div(id="ph3_7"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph3-ph7input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph7", color="primary", className="me-1", id="ph3-ph7-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph10"),html.Div(id="ph3_10"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph3-ph10input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph10", color="primary", className="me-1", id="ph3-ph10-button", n_clicks=0),),),

                     ]),),
                dbc.ModalFooter(
                    dbc.Button("CLOSE BUTTON", id="close3", className="ml-auto")
                ),
            ],
            id="modal3",
        ),
        ],)


@app.callback([Output('ph3status', 'children',allow_duplicate=True),
              Output('ph3_4', 'children',allow_duplicate=True),
              Output('ph3_7', 'children',allow_duplicate=True),
              Output('ph3_10', 'children',allow_duplicate=True),],[
              Input("ph3-sens-button", 'n_clicks')],
              prevent_initial_call=True)
def calib3(n_clicks):
    ph3status = database.return_latest_sensordata_item(2)
    ph3_4 = database.return_latest_phcalibs_item(6)
    ph3_7 = database.return_latest_phcalibs_item(7)
    ph3_10 = database.return_latest_phcalibs_item(8)
    return ph3status,ph3_4,ph3_7,ph3_10

@app.callback([Output('ph3status', 'children',allow_duplicate=True),
              Output('ph3_4', 'children',allow_duplicate=True),
              Output('ph3_7', 'children',allow_duplicate=True),
              Output('ph3_10', 'children',allow_duplicate=True),],[
              Input("ph3-ph4-button", 'n_clicks')],
              State('ph3-ph4input','value'),
              prevent_initial_call=True)
def calib3ph4(n_clicks,value):
    database.calibrate_sensor_ph4(3,value)
    ph3status = database.return_latest_sensordata_item(2)
    ph3_4 = database.return_latest_phcalibs_item(6)
    ph3_7 = database.return_latest_phcalibs_item(7)
    ph3_10 = database.return_latest_phcalibs_item(8)
    return ph3status,ph3_4,ph3_7,ph3_10

@app.callback([Output('ph3status', 'children',allow_duplicate=True),
              Output('ph3_4', 'children',allow_duplicate=True),
              Output('ph3_7', 'children',allow_duplicate=True),
              Output('ph3_10', 'children',allow_duplicate=True),],[
              Input("ph3-ph7-button", 'n_clicks')],
              State('ph3-ph7input','value'),
              prevent_initial_call=True)
def calib3ph7(n_clicks,value):
    database.calibrate_sensor_ph7(3,value)
    ph3status = database.return_latest_sensordata_item(2)
    ph3_4 = database.return_latest_phcalibs_item(6)
    ph3_7 = database.return_latest_phcalibs_item(7)
    ph3_10 = database.return_latest_phcalibs_item(8)
    return ph3status,ph3_4,ph3_7,ph3_10

@app.callback([Output('ph3status', 'children',allow_duplicate=True),
              Output('ph3_4', 'children',allow_duplicate=True),
              Output('ph3_7', 'children',allow_duplicate=True),
              Output('ph3_10', 'children',allow_duplicate=True),],[
              Input("ph3-ph10-button", 'n_clicks')],
              State('ph3-ph10input','value'),
              prevent_initial_call=True)
def calib3ph10(n_clicks,value):
    database.calibrate_sensor_ph10(3,value)
    ph3status = database.return_latest_sensordata_item(2)
    ph3_4 = database.return_latest_phcalibs_item(6)
    ph3_7 = database.return_latest_phcalibs_item(7)
    ph3_10 = database.return_latest_phcalibs_item(8)
    return ph3status,ph3_4,ph3_7,ph3_10

@app.callback(
    Output("modal3", "is_open"),
    [Input("ph3calibbutton", "n_clicks"), Input("close3", "n_clicks")],
    [State("modal3", "is_open")],
)
def toggle_modal3(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open

@app.callback(
         Output('ph3setpoint', 'children'),
         Input('ph3button', 'n_clicks'),
         State('ph3-input','value')
        )
def on_button_clickph3(n_clicks,value):
    if n_clicks == 0:
        latestvalue = database.return_latest_commands_item(2)
        return latestvalue
    database.add_one_commands(2,value)
    return value

ph4screen = dbc.Col(
    [dbc.Row(html.Div("pH-sensor 4"),),
        dbc.Row(html.Div(id="ph4live"),),
        dbc.Row(html.Div("Valve open if pH > than"),),
        dbc.Row(html.Div(id="ph4setpoint"),),
        dbc.Row(html.Div([dcc.Input(id="ph4-input", type="number", value="", min=0, max=14, step=0.1),
        dbc.FormText("Valve setpoint (0-14, 0.1 step)"),]),),
        dbc.Row(html.Div(dbc.Button("Update setpoint", color="primary", className="me-1", id="ph4button", n_clicks=0),),),
        dbc.Row(html.Div(dbc.Button("open calibration", color="primary", className="me-1", id="ph4calibbutton", n_clicks=0),),),
dbc.Modal(
            [
                dbc.ModalHeader("PH-calibration"),
                dbc.ModalBody(dbc.Col([
                                  dbc.Row([html.Div("current (sensor 4) reading"),html.Div(id="ph4status"),]),
                                  dbc.Row(html.Div(dbc.Button("Update sensor reading", color="primary", className="me-1", id="ph4-sens-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph4"),html.Div(id="ph4_4"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph4-ph4input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph4", color="primary", className="me-1", id="ph4-ph4-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph7"),html.Div(id="ph4_7"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph4-ph7input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph7", color="primary", className="me-1", id="ph4-ph7-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph10"),html.Div(id="ph4_10"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph4-ph10input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph10", color="primary", className="me-1", id="ph4-ph10-button", n_clicks=0),),),

                     ]),),
                dbc.ModalFooter(
                    dbc.Button("CLOSE BUTTON", id="close4", className="ml-auto")
                ),
            ],
            id="modal4",
        ),
        ],)

@app.callback([Output('ph4status', 'children',allow_duplicate=True),
              Output('ph4_4', 'children',allow_duplicate=True),
              Output('ph4_7', 'children',allow_duplicate=True),
              Output('ph4_10', 'children',allow_duplicate=True),],[
              Input("ph4-sens-button", 'n_clicks')],
              prevent_initial_call=True)
def calib4(n_clicks):
    ph4status = database.return_latest_sensordata_item(3)
    ph4_4 = database.return_latest_phcalibs_item(9)
    ph4_7 = database.return_latest_phcalibs_item(10)
    ph4_10 = database.return_latest_phcalibs_item(11)
    return ph4status,ph4_4,ph4_7,ph4_10

@app.callback([Output('ph4status', 'children',allow_duplicate=True),
              Output('ph4_4', 'children',allow_duplicate=True),
              Output('ph4_7', 'children',allow_duplicate=True),
              Output('ph4_10', 'children',allow_duplicate=True),],[
              Input("ph4-ph4-button", 'n_clicks')],
              State('ph4-ph4input','value'),
              prevent_initial_call=True)
def calib4ph4(n_clicks,value):
    database.calibrate_sensor_ph4(4,value)
    ph4status = database.return_latest_sensordata_item(3)
    ph4_4 = database.return_latest_phcalibs_item(9)
    ph4_7 = database.return_latest_phcalibs_item(10)
    ph4_10 = database.return_latest_phcalibs_item(11)
    return ph4status,ph4_4,ph4_7,ph4_10

@app.callback([Output('ph4status', 'children',allow_duplicate=True),
              Output('ph4_4', 'children',allow_duplicate=True),
              Output('ph4_7', 'children',allow_duplicate=True),
              Output('ph4_10', 'children',allow_duplicate=True),],[
              Input("ph4-ph7-button", 'n_clicks')],
              State('ph4-ph7input','value'),
              prevent_initial_call=True)
def calib4ph7(n_clicks,value):
    database.calibrate_sensor_ph7(4,value)
    ph4status = database.return_latest_sensordata_item(3)
    ph4_4 = database.return_latest_phcalibs_item(9)
    ph4_7 = database.return_latest_phcalibs_item(10)
    ph4_10 = database.return_latest_phcalibs_item(11)
    return ph4status,ph4_4,ph4_7,ph4_10

@app.callback([Output('ph4status', 'children',allow_duplicate=True),
              Output('ph4_4', 'children',allow_duplicate=True),
              Output('ph4_7', 'children',allow_duplicate=True),
              Output('ph4_10', 'children',allow_duplicate=True),],[
              Input("ph4-ph10-button", 'n_clicks')],
              State('ph4-ph10input','value'),
              prevent_initial_call=True)
def calib4ph10(n_clicks,value):
    database.calibrate_sensor_ph10(4,value)
    ph4status = database.return_latest_sensordata_item(3)
    ph4_4 = database.return_latest_phcalibs_item(9)
    ph4_7 = database.return_latest_phcalibs_item(10)
    ph4_10 = database.return_latest_phcalibs_item(11)
    return ph4status,ph4_4,ph4_7,ph4_10

@app.callback(
    Output("modal4", "is_open"),
    [Input("ph4calibbutton", "n_clicks"), Input("close4", "n_clicks")],
    [State("modal4", "is_open")],
)
def toggle_modal4(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open
@app.callback(
         Output('ph4setpoint', 'children'),
         Input('ph4button', 'n_clicks'),
         State('ph4-input','value')
        )
def on_button_clickph4(n_clicks,value):
    if n_clicks == 0:
        latestvalue = database.return_latest_commands_item(3)
        return latestvalue
    database.add_one_commands(3,value)
    return value

ph5screen = dbc.Col(
    [dbc.Row(html.Div("pH-sensor 5"),),
        dbc.Row(html.Div(id="ph5live"),),
        dbc.Row(html.Div("Valve open if pH > than"),),
        dbc.Row(html.Div(id="ph5setpoint"),),
        dbc.Row(html.Div([dcc.Input(id="ph5-input", type="number", value="", min=0, max=14, step=0.1),
        dbc.FormText("Valve setpoint (0-14, 0.1 step)"),]),),
        dbc.Row(html.Div(dbc.Button("Update setpoint", color="primary", className="me-1", id="ph5button", n_clicks=0),),),
        dbc.Row(html.Div(dbc.Button("open calibration", color="primary", className="me-1", id="ph5calibbutton", n_clicks=0),),),
dbc.Modal(
            [
                dbc.ModalHeader("PH-calibration"),
                dbc.ModalBody(dbc.Col([
                                  dbc.Row([html.Div("current (sensor 5) reading"),html.Div(id="ph5status"),]),
                                  dbc.Row(html.Div(dbc.Button("Update sensor reading", color="primary", className="me-1", id="ph5-sens-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph4"),html.Div(id="ph5_4"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph5-ph4input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph4", color="primary", className="me-1", id="ph5-ph4-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph7"),html.Div(id="ph5_7"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph5-ph7input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph7", color="primary", className="me-1", id="ph5-ph7-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph10"),html.Div(id="ph5_10"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph5-ph10input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph10", color="primary", className="me-1", id="ph5-ph10-button", n_clicks=0),),),

                     ]),),
                dbc.ModalFooter(
                    dbc.Button("CLOSE BUTTON", id="close5", className="ml-auto")
                ),
            ],
            id="modal5",
        ),
        ],)

@app.callback([Output('ph5status', 'children',allow_duplicate=True),
              Output('ph5_4', 'children',allow_duplicate=True),
              Output('ph5_7', 'children',allow_duplicate=True),
              Output('ph5_10', 'children',allow_duplicate=True),],[
              Input("ph5-sens-button", 'n_clicks')],
              prevent_initial_call=True)
def calib5(n_clicks):
    ph5status = database.return_latest_sensordata_item(4)
    ph5_4 = database.return_latest_phcalibs_item(12)
    ph5_7 = database.return_latest_phcalibs_item(13)
    ph5_10 = database.return_latest_phcalibs_item(14)
    return ph5status,ph5_4,ph5_7,ph5_10

@app.callback([Output('ph5status', 'children',allow_duplicate=True),
              Output('ph5_4', 'children',allow_duplicate=True),
              Output('ph5_7', 'children',allow_duplicate=True),
              Output('ph5_10', 'children',allow_duplicate=True),],[
              Input("ph5-ph4-button", 'n_clicks')],
              State('ph5-ph4input','value'),
              prevent_initial_call=True)
def calib5ph4(n_clicks,value):
    database.calibrate_sensor_ph4(5,value)
    ph5status = database.return_latest_sensordata_item(4)
    ph5_4 = database.return_latest_phcalibs_item(12)
    ph5_7 = database.return_latest_phcalibs_item(13)
    ph5_10 = database.return_latest_phcalibs_item(14)
    return ph5status,ph5_4,ph5_7,ph5_10

@app.callback([Output('ph5status', 'children',allow_duplicate=True),
              Output('ph5_4', 'children',allow_duplicate=True),
              Output('ph5_7', 'children',allow_duplicate=True),
              Output('ph5_10', 'children',allow_duplicate=True),],[
              Input("ph5-ph7-button", 'n_clicks')],
              State('ph5-ph7input','value'),
              prevent_initial_call=True)
def calib5ph7(n_clicks,value):
    database.calibrate_sensor_ph7(5,value)
    ph5status = database.return_latest_sensordata_item(4)
    ph5_4 = database.return_latest_phcalibs_item(12)
    ph5_7 = database.return_latest_phcalibs_item(13)
    ph5_10 = database.return_latest_phcalibs_item(14)
    return ph5status,ph5_4,ph5_7,ph5_10

@app.callback([Output('ph5status', 'children',allow_duplicate=True),
              Output('ph5_4', 'children',allow_duplicate=True),
              Output('ph5_7', 'children',allow_duplicate=True),
              Output('ph5_10', 'children',allow_duplicate=True),],[
              Input("ph5-ph10-button", 'n_clicks')],
              State('ph5-ph10input','value'),
              prevent_initial_call=True)
def calib5ph10(n_clicks,value):
    database.calibrate_sensor_ph10(5,value)
    ph5status = database.return_latest_sensordata_item(4)
    ph5_4 = database.return_latest_phcalibs_item(12)
    ph5_7 = database.return_latest_phcalibs_item(13)
    ph5_10 = database.return_latest_phcalibs_item(14)
    return ph5status,ph5_4,ph5_7,ph5_10

@app.callback(
    Output("modal5", "is_open"),
    [Input("ph5calibbutton", "n_clicks"), Input("close5", "n_clicks")],
    [State("modal5", "is_open")],
)
def toggle_modal5(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open

@app.callback(
         Output('ph5setpoint', 'children'),
         Input('ph5button', 'n_clicks'),
         State('ph5-input','value')
        )
def on_button_clickph5(n_clicks,value):
    if n_clicks == 0:
        latestvalue = database.return_latest_commands_item(4)
        return latestvalue
    database.add_one_commands(4,value)
    return value

ph6screen = dbc.Col(
    [dbc.Row(html.Div("pH-sensor 6"),),
        dbc.Row(html.Div(id="ph6live"),),
        dbc.Row(html.Div("Valve open if pH > than"),),
        dbc.Row(html.Div(id="ph6setpoint"),),
        dbc.Row(html.Div([dcc.Input(id="ph6-input", type="number", value="", min=0, max=14, step=0.1),
        dbc.FormText("Valve setpoint (0-14, 0.1 step)"),]),),
        dbc.Row(html.Div(dbc.Button("Update setpoint", color="primary", className="me-1", id="ph6button", n_clicks=0),),),
        dbc.Row(html.Div(dbc.Button("open calibration", color="primary", className="me-1", id="ph6calibbutton", n_clicks=0),),),
        dbc.Modal(
            [
                dbc.ModalHeader("PH-calibration"),
                dbc.ModalBody(dbc.Col([
                                  dbc.Row([html.Div("current (sensor 6) reading"),html.Div(id="ph6status"),]),
                                  dbc.Row(html.Div(dbc.Button("Update sensor reading", color="primary", className="me-1", id="ph6-sens-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph4"),html.Div(id="ph6_4"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph6-ph4input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph4", color="primary", className="me-1", id="ph6-ph4-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph7"),html.Div(id="ph6_7"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph6-ph7input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph7", color="primary", className="me-1", id="ph6-ph7-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph10"),html.Div(id="ph6_10"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph6-ph10input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph10", color="primary", className="me-1", id="ph6-ph10-button", n_clicks=0),),),

                     ]),),
                dbc.ModalFooter(
                    dbc.Button("CLOSE BUTTON", id="close6", className="ml-auto")
                ),
            ],
            id="modal6",
        ),
        ],)

@app.callback([Output('ph6status', 'children',allow_duplicate=True),
              Output('ph6_4', 'children',allow_duplicate=True),
              Output('ph6_7', 'children',allow_duplicate=True),
              Output('ph6_10', 'children',allow_duplicate=True),],[
              Input("ph6-sens-button", 'n_clicks')],
              prevent_initial_call=True)
def calib6(n_clicks):
    ph6status = database.return_latest_sensordata_item(5)
    ph6_4 = database.return_latest_phcalibs_item(15)
    ph6_7 = database.return_latest_phcalibs_item(16)
    ph6_10 = database.return_latest_phcalibs_item(17)
    return ph6status,ph6_4,ph6_7,ph6_10

@app.callback([Output('ph6status', 'children',allow_duplicate=True),
              Output('ph6_4', 'children',allow_duplicate=True),
              Output('ph6_7', 'children',allow_duplicate=True),
              Output('ph6_10', 'children',allow_duplicate=True),],[
              Input("ph6-ph4-button", 'n_clicks')],
              State('ph6-ph4input','value'),
              prevent_initial_call=True)
def calib6ph4(n_clicks,value):
    database.calibrate_sensor_ph4(6,value)
    ph6status = database.return_latest_sensordata_item(5)
    ph6_4 = database.return_latest_phcalibs_item(15)
    ph6_7 = database.return_latest_phcalibs_item(16)
    ph6_10 = database.return_latest_phcalibs_item(17)
    return ph6status,ph6_4,ph6_7,ph6_10

@app.callback([Output('ph6status', 'children',allow_duplicate=True),
              Output('ph6_4', 'children',allow_duplicate=True),
              Output('ph6_7', 'children',allow_duplicate=True),
              Output('ph6_10', 'children',allow_duplicate=True),],[
              Input("ph6-ph7-button", 'n_clicks')],
              State('ph6-ph7input','value'),
              prevent_initial_call=True)
def calib6ph7(n_clicks,value):
    database.calibrate_sensor_ph7(6,value)
    ph6status = database.return_latest_sensordata_item(5)
    ph6_4 = database.return_latest_phcalibs_item(15)
    ph6_7 = database.return_latest_phcalibs_item(16)
    ph6_10 = database.return_latest_phcalibs_item(17)
    return ph6status,ph6_4,ph6_7,ph6_10

@app.callback([Output('ph6status', 'children',allow_duplicate=True),
              Output('ph6_4', 'children',allow_duplicate=True),
              Output('ph6_7', 'children',allow_duplicate=True),
              Output('ph6_10', 'children',allow_duplicate=True),],[
              Input("ph6-ph10-button", 'n_clicks')],
              State('ph6-ph10input','value'),
              prevent_initial_call=True)
def calib6ph10(n_clicks,value):
    database.calibrate_sensor_ph10(6,value)
    ph6status = database.return_latest_sensordata_item(5)
    ph6_4 = database.return_latest_phcalibs_item(15)
    ph6_7 = database.return_latest_phcalibs_item(16)
    ph6_10 = database.return_latest_phcalibs_item(17)
    return ph6status,ph6_4,ph6_7,ph6_10

@app.callback(
    Output("modal6", "is_open"),
    [Input("ph6calibbutton", "n_clicks"), Input("close6", "n_clicks")],
    [State("modal6", "is_open")],
)
def toggle_modal6(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open

@app.callback(
         Output('ph6setpoint', 'children'),
         Input('ph6button', 'n_clicks'),
         State('ph6-input','value')
        )
def on_button_clickph6(n_clicks,value):
    if n_clicks == 0:
        latestvalue = database.return_latest_commands_item(5)
        return latestvalue
    database.add_one_commands(5,value)
    return value

ph7screen = dbc.Col(
    [dbc.Row(html.Div("pH-sensor 7"),),
        dbc.Row(html.Div(id="ph7live"),),
        dbc.Row(html.Div("Valve open if pH > than"),),
        dbc.Row(html.Div(id="ph7setpoint"),),
        dbc.Row(html.Div([dcc.Input(id="ph7-input", type="number", value="", min=0, max=14, step=0.1),
        dbc.FormText("Valve setpoint (0-14, 0.1 step)"),]),),
        dbc.Row(html.Div(dbc.Button("Update setpoint", color="primary", className="me-1", id="ph7button", n_clicks=0),),),
        dbc.Row(html.Div(dbc.Button("open calibration", color="primary", className="me-1", id="ph7calibbutton", n_clicks=0),),),
dbc.Modal(
            [
                dbc.ModalHeader("PH-calibration"),
                dbc.ModalBody(dbc.Col([
                                  dbc.Row([html.Div("current (sensor 7) reading"),html.Div(id="ph7status"),]),
                                  dbc.Row(html.Div(dbc.Button("Update sensor reading", color="primary", className="me-1", id="ph7-sens-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph4"),html.Div(id="ph7_4"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph7-ph4input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph4", color="primary", className="me-1", id="ph7-ph4-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph7"),html.Div(id="ph7_7"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph7-ph7input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph7", color="primary", className="me-1", id="ph7-ph7-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph10"),html.Div(id="ph7_10"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph7-ph10input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph10", color="primary", className="me-1", id="ph7-ph10-button", n_clicks=0),),),

                     ]),),
                dbc.ModalFooter(
                    dbc.Button("CLOSE BUTTON", id="close7", className="ml-auto")
                ),
            ],
            id="modal7",
        ),
        ],)

@app.callback([Output('ph7status', 'children',allow_duplicate=True),
              Output('ph7_4', 'children',allow_duplicate=True),
              Output('ph7_7', 'children',allow_duplicate=True),
              Output('ph7_10', 'children',allow_duplicate=True),],[
              Input("ph7-sens-button", 'n_clicks')],
              prevent_initial_call=True)
def calib7(n_clicks):
    ph7status = database.return_latest_sensordata_item(6)
    ph7_4 = database.return_latest_phcalibs_item(18)
    ph7_7 = database.return_latest_phcalibs_item(19)
    ph7_10 = database.return_latest_phcalibs_item(20)
    return ph7status,ph7_4,ph7_7,ph7_10

@app.callback([Output('ph7status', 'children',allow_duplicate=True),
              Output('ph7_4', 'children',allow_duplicate=True),
              Output('ph7_7', 'children',allow_duplicate=True),
              Output('ph7_10', 'children',allow_duplicate=True),],[
              Input("ph7-ph4-button", 'n_clicks')],
              State('ph7-ph4input','value'),
              prevent_initial_call=True)
def calib7ph4(n_clicks,value):
    database.calibrate_sensor_ph4(7,value)
    ph7status = database.return_latest_sensordata_item(6)
    ph7_4 = database.return_latest_phcalibs_item(18)
    ph7_7 = database.return_latest_phcalibs_item(19)
    ph7_10 = database.return_latest_phcalibs_item(20)
    return ph7status,ph7_4,ph7_7,ph7_10

@app.callback([Output('ph7status', 'children',allow_duplicate=True),
              Output('ph7_4', 'children',allow_duplicate=True),
              Output('ph7_7', 'children',allow_duplicate=True),
              Output('ph7_10', 'children',allow_duplicate=True),],[
              Input("ph7-ph7-button", 'n_clicks')],
              State('ph7-ph7input','value'),
              prevent_initial_call=True)
def calib7ph7(n_clicks,value):
    database.calibrate_sensor_ph7(7,value)
    ph7status = database.return_latest_sensordata_item(6)
    ph7_4 = database.return_latest_phcalibs_item(18)
    ph7_7 = database.return_latest_phcalibs_item(19)
    ph7_10 = database.return_latest_phcalibs_item(20)
    return ph7status,ph7_4,ph7_7,ph7_10

@app.callback([Output('ph7status', 'children',allow_duplicate=True),
              Output('ph7_4', 'children',allow_duplicate=True),
              Output('ph7_7', 'children',allow_duplicate=True),
              Output('ph7_10', 'children',allow_duplicate=True),],[
              Input("ph7-ph10-button", 'n_clicks')],
              State('ph7-ph10input','value'),
              prevent_initial_call=True)
def calib7ph10(n_clicks,value):
    database.calibrate_sensor_ph10(7,value)
    ph7status = database.return_latest_sensordata_item(6)
    ph7_4 = database.return_latest_phcalibs_item(18)
    ph7_7 = database.return_latest_phcalibs_item(19)
    ph7_10 = database.return_latest_phcalibs_item(20)
    return ph7status,ph7_4,ph7_7,ph7_10

@app.callback(
    Output("modal7", "is_open"),
    [Input("ph7calibbutton", "n_clicks"), Input("close7", "n_clicks")],
    [State("modal7", "is_open")],
)
def toggle_modal7(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open


@app.callback(
         Output('ph7setpoint', 'children'),
         Input('ph7button', 'n_clicks'),
         State('ph7-input','value')
        )
def on_button_clickph7(n_clicks,value):
    if n_clicks == 0:
        latestvalue = database.return_latest_commands_item(6)
        return latestvalue
    database.add_one_commands(6,value)
    return value

ph8screen = dbc.Col(
    [dbc.Row(html.Div("pH-sensor 8"),),
        dbc.Row(html.Div(id="ph8live"),),
        dbc.Row(html.Div("Valve open if pH > than"),),
        dbc.Row(html.Div(id="ph8setpoint"),),
        dbc.Row(html.Div([dcc.Input(id="ph8-input", type="number", value="", min=0, max=14, step=0.1),
        dbc.FormText("Valve setpoint (0-14, 0.1 step)"),]),),
        dbc.Row(html.Div(dbc.Button("Update setpoint", color="primary", className="me-1", id="ph8button", n_clicks=0),),),
        dbc.Row(html.Div(dbc.Button("open calibration", color="primary", className="me-1", id="ph8calibbutton", n_clicks=0),),),
     dbc.Modal(
            [
                dbc.ModalHeader("PH-calibration"),
        dbc.ModalBody(dbc.Col([
                                  dbc.Row([html.Div("current (sensor 8) reading"),html.Div(id="ph8status"),]),
                                  dbc.Row(html.Div(dbc.Button("Update sensor reading", color="primary", className="me-1", id="ph8-sens-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph4"),html.Div(id="ph8_4"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph8-ph4input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph4", color="primary", className="me-1", id="ph8-ph4-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph7"),html.Div(id="ph8_7"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph8-ph7input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph7", color="primary", className="me-1", id="ph8-ph7-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph10"),html.Div(id="ph8_10"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph8-ph10input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph10", color="primary", className="me-1", id="ph8-ph10-button", n_clicks=0),),),

                     ]),),
                dbc.ModalFooter(
                    dbc.Button("CLOSE BUTTON", id="close8", className="ml-auto")
            ),
        ],
        id="modal8",
    ),
    ],)

@app.callback([Output('ph8status', 'children',allow_duplicate=True),
              Output('ph8_4', 'children',allow_duplicate=True),
              Output('ph8_7', 'children',allow_duplicate=True),
              Output('ph8_10', 'children',allow_duplicate=True),],[
              Input("ph8-sens-button", 'n_clicks')],
              prevent_initial_call=True)
def calib8(n_clicks):
    ph8status = database.return_latest_sensordata_item(7)
    ph8_4 = database.return_latest_phcalibs_item(21)
    ph8_7 = database.return_latest_phcalibs_item(22)
    ph8_10 = database.return_latest_phcalibs_item(23)
    return ph8status,ph8_4,ph8_7,ph8_10

@app.callback([Output('ph8status', 'children',allow_duplicate=True),
              Output('ph8_4', 'children',allow_duplicate=True),
              Output('ph8_7', 'children',allow_duplicate=True),
              Output('ph8_10', 'children',allow_duplicate=True),],[
              Input("ph8-ph4-button", 'n_clicks')],
              State('ph8-ph4input','value'),
              prevent_initial_call=True)
def calib8ph4(n_clicks,value):
    database.calibrate_sensor_ph4(8,value)
    ph8status = database.return_latest_sensordata_item(7)
    ph8_4 = database.return_latest_phcalibs_item(21)
    ph8_7 = database.return_latest_phcalibs_item(22)
    ph8_10 = database.return_latest_phcalibs_item(23)
    return ph8status,ph8_4,ph8_7,ph8_10

@app.callback([Output('ph8status', 'children',allow_duplicate=True),
              Output('ph8_4', 'children',allow_duplicate=True),
              Output('ph8_7', 'children',allow_duplicate=True),
              Output('ph8_10', 'children',allow_duplicate=True),],[
              Input("ph8-ph7-button", 'n_clicks')],
              State('ph8-ph7input','value'),
              prevent_initial_call=True)
def calib8ph7(n_clicks,value):
    database.calibrate_sensor_ph7(8,value)
    ph8status = database.return_latest_sensordata_item(7)
    ph8_4 = database.return_latest_phcalibs_item(21)
    ph8_7 = database.return_latest_phcalibs_item(22)
    ph8_10 = database.return_latest_phcalibs_item(23)
    return ph8status,ph8_4,ph8_7,ph8_10

@app.callback([Output('ph8status', 'children',allow_duplicate=True),
              Output('ph8_4', 'children',allow_duplicate=True),
              Output('ph8_7', 'children',allow_duplicate=True),
              Output('ph8_10', 'children',allow_duplicate=True),],[
              Input("ph8-ph10-button", 'n_clicks')],
              State('ph8-ph10input','value'),
              prevent_initial_call=True)
def calib8ph10(n_clicks,value):
    database.calibrate_sensor_ph10(8,value)
    ph8status = database.return_latest_sensordata_item(7)
    ph8_4 = database.return_latest_phcalibs_item(21)
    ph8_7 = database.return_latest_phcalibs_item(22)
    ph8_10 = database.return_latest_phcalibs_item(23)
    return ph8status,ph8_4,ph8_7,ph8_10

@app.callback(
    Output("modal8", "is_open"),
    [Input("ph8calibbutton", "n_clicks"), Input("close8", "n_clicks")],
    [State("modal8", "is_open")],
)
def toggle_modal8(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open

@app.callback(
         Output('ph8setpoint', 'children'),
         Input('ph8button', 'n_clicks'),
         State('ph8-input','value')
        )
def on_button_clickph8(n_clicks,value):
    if n_clicks == 0:
        latestvalue = database.return_latest_commands_item(7)
        return latestvalue
    database.add_one_commands(7,value)
    return value

ph9screen = dbc.Col(
    [dbc.Row(html.Div("pH-sensor 9"),),
        dbc.Row(html.Div(id="ph9live"),),
        dbc.Row(html.Div("Valve open if pH > than"),),
        dbc.Row(html.Div(id="ph9setpoint"),),
        dbc.Row(html.Div([dcc.Input(id="ph9-input", type="number", value="", min=0, max=14, step=0.1),
        dbc.FormText("Valve setpoint (0-14, 0.1 step)"),]),),
        dbc.Row(html.Div(dbc.Button("Update setpoint", color="primary", className="me-1", id="ph9button", n_clicks=0),),),
        dbc.Row(html.Div(dbc.Button("open calibration", color="primary", className="me-1", id="ph9calibbutton", n_clicks=0),),),
     dbc.Modal(
            [
                dbc.ModalHeader("PH-calibration"),
                dbc.ModalBody(dbc.Col([
                                  dbc.Row([html.Div("current (sensor 9) reading"),html.Div(id="ph9status"),]),
                                  dbc.Row(html.Div(dbc.Button("Update sensor reading", color="primary", className="me-1", id="ph9-sens-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph4"),html.Div(id="ph9_4"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph9-ph4input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph4", color="primary", className="me-1", id="ph9-ph4-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph7"),html.Div(id="ph9_7"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph9-ph7input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph7", color="primary", className="me-1", id="ph9-ph7-button", n_clicks=0),),),
                                  dbc.Row([html.Div("current value at ph10"),html.Div(id="ph9_10"),]),
                                  dbc.Row(html.Div(dcc.Input(id="ph9-ph10input", type="number", value="", min=0, max=1023, step=1),),),
                                  dbc.Row(html.Div(dbc.Button("Update value at ph10", color="primary", className="me-1", id="ph9-ph10-button", n_clicks=0),),),

                     ]),),
                dbc.ModalFooter(
                    dbc.Button("CLOSE BUTTON", id="close9", className="ml-auto")
            ),
        ],
        id="modal9",
    ),
    ],)

@app.callback([Output('ph9status', 'children',allow_duplicate=True),
              Output('ph9_4', 'children',allow_duplicate=True),
              Output('ph9_7', 'children',allow_duplicate=True),
              Output('ph9_10', 'children',allow_duplicate=True),],[
              Input("ph9-sens-button", 'n_clicks')],
              prevent_initial_call=True)
def calib9(n_clicks):
    ph9status = database.return_latest_sensordata_item(8)
    ph9_4 = database.return_latest_phcalibs_item(24)
    ph9_7 = database.return_latest_phcalibs_item(25)
    ph9_10 = database.return_latest_phcalibs_item(26)
    return ph9status,ph9_4,ph9_7,ph9_10

@app.callback([Output('ph9status', 'children',allow_duplicate=True),
              Output('ph9_4', 'children',allow_duplicate=True),
              Output('ph9_7', 'children',allow_duplicate=True),
              Output('ph9_10', 'children',allow_duplicate=True),],[
              Input("ph9-ph4-button", 'n_clicks')],
              State('ph9-ph4input','value'),
              prevent_initial_call=True)
def calib9ph4(n_clicks,value):
    database.calibrate_sensor_ph4(9,value)
    ph9status = database.return_latest_sensordata_item(8)
    ph9_4 = database.return_latest_phcalibs_item(24)
    ph9_7 = database.return_latest_phcalibs_item(25)
    ph9_10 = database.return_latest_phcalibs_item(26)
    return ph9status,ph9_4,ph9_7,ph9_10

@app.callback([Output('ph9status', 'children',allow_duplicate=True),
              Output('ph9_4', 'children',allow_duplicate=True),
              Output('ph9_7', 'children',allow_duplicate=True),
              Output('ph9_10', 'children',allow_duplicate=True),],[
              Input("ph9-ph7-button", 'n_clicks')],
              State('ph9-ph7input','value'),
              prevent_initial_call=True)
def calib9ph7(n_clicks,value):
    database.calibrate_sensor_ph7(9,value)
    ph9status = database.return_latest_sensordata_item(8)
    ph9_4 = database.return_latest_phcalibs_item(24)
    ph9_7 = database.return_latest_phcalibs_item(25)
    ph9_10 = database.return_latest_phcalibs_item(26)
    return ph9status,ph9_4,ph9_7,ph9_10

@app.callback([Output('ph9status', 'children',allow_duplicate=True),
              Output('ph9_4', 'children',allow_duplicate=True),
              Output('ph9_7', 'children',allow_duplicate=True),
              Output('ph9_10', 'children',allow_duplicate=True),],[
              Input("ph9-ph10-button", 'n_clicks')],
              State('ph9-ph10input','value'),
              prevent_initial_call=True)
def calib9ph10(n_clicks,value):
    database.calibrate_sensor_ph10(9,value)
    ph9status = database.return_latest_sensordata_item(8)
    ph9_4 = database.return_latest_phcalibs_item(24)
    ph9_7 = database.return_latest_phcalibs_item(25)
    ph9_10 = database.return_latest_phcalibs_item(26)
    return ph9status,ph9_4,ph9_7,ph9_10

@app.callback(
    Output("modal9", "is_open"),
    [Input("ph9calibbutton", "n_clicks"), Input("close9", "n_clicks")],
    [State("modal9", "is_open")],
)
def toggle_modal9(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open

@app.callback(
         Output('ph9setpoint', 'children'),
         Input('ph9button', 'n_clicks'),
         State('ph9-input','value')
        )
def on_button_clickph9(n_clicks,value):
    if n_clicks == 0:
        latestvalue = database.return_latest_commands_item(8)
        return latestvalue
    database.add_one_commands(8,value)
    return value

valve1screen = dbc.Col(
    [dbc.Row(html.Div("valve1"),),
     dbc.Row(html.Div(id='valve1'),),
],)
valve2screen = dbc.Col(
    [dbc.Row(html.Div("valve2"),),
     dbc.Row(html.Div(id='valve2'),),
],)
valve3screen = dbc.Col(
    [dbc.Row(html.Div("valve3"),),
     dbc.Row(html.Div(id='valve3'),),
],)
valve4screen = dbc.Col(
    [dbc.Row(html.Div("valve4"),),
     dbc.Row(html.Div(id='valve4'),),
],)
valve5screen = dbc.Col(
    [dbc.Row(html.Div("valve5"),),
     dbc.Row(html.Div(id='valve5'),),
],)
valve6screen = dbc.Col(
    [dbc.Row(html.Div("valve6"),),
     dbc.Row(html.Div(id='valve6'),),
],)
valve7screen = dbc.Col(
    [dbc.Row(html.Div("valve7"),),
     dbc.Row(html.Div(id='valve7'),),
],)
valve8screen = dbc.Col(
    [dbc.Row(html.Div("valve8"),),
     dbc.Row(html.Div(id='valve8'),),
],)
valve9screen = dbc.Col(
    [dbc.Row(html.Div("valve9"),),
     dbc.Row(html.Div(id='valve9'),),
],)


@app.callback([Output('valve1', 'children'),
               Output('valve2', 'children'),
               Output('valve3', 'children'),
               Output('valve4', 'children'),
               Output('valve5', 'children'),
               Output('valve6', 'children'),
               Output('valve7', 'children'),
               Output('valve8', 'children'),
               Output('valve9', 'children'),],
               Input('ph9live','children'))
def updatevalve(n):
    status1 = database.return_latest_commands_item(9)
    status2 = database.return_latest_commands_item(10)
    status3 = database.return_latest_commands_item(11)
    status4 = database.return_latest_commands_item(12)
    status5 = database.return_latest_commands_item(13)
    status6 = database.return_latest_commands_item(14)
    status7 = database.return_latest_commands_item(15)
    status8 = database.return_latest_commands_item(16)
    status9 = database.return_latest_commands_item(17)
    return status1,status2,status3,status4,status5,status6,status7,status8,status9


temp1screen = dbc.Col(
             [dbc.Row(html.Div("Temp 1"),),
              dbc.Row(html.Div(id='tempe1'),),
],)

temp2screen = dbc.Col([dbc.Row(html.Div("Temp 1"),),
              dbc.Row(html.Div(id='tempe2'),),],)


temp3screen = dbc.Col([dbc.Row(html.Div("Temp 1"),),
              dbc.Row(html.Div(id='tempe3'),),],)

@app.callback([Output('tempe1', 'children'),
              Output('tempe2', 'children'),
              Output('tempe3', 'children'),],
              Input('ph8live', 'children'))
def updatetemp(n):
    status1 = database.return_latest_sensordata_item(9)
    status2 = database.return_latest_sensordata_item(10)
    status3 = database.return_latest_sensordata_item(11)

    return status1,status2,status3



tab_controldiagram = html.Div([
dbc.Row(html.H3('Tabcontroldiag')),  
dbc.Row(html.H1('pH-measurements')),
dbc.Row([ph1screen,ph2screen,ph3screen,ph4screen,ph5screen,ph6screen,ph7screen,ph8screen,ph9screen,
]),
dbc.Row(html.H1('Valvestatus')),
dbc.Row([valve1screen,valve2screen,valve3screen,valve4screen,valve5screen,valve6screen,valve7screen,valve8screen,valve9screen,
]),

dbc.Row(html.H1('Temperature-measurements')),
dbc.Row([temp1screen,temp2screen,temp3screen,
         ]),
]),






pil_image = Image.open("assets/spam.jpg")

tab_programs= [html.H3('TabPrograms'),
html.Img(src=pil_image)
               ]

tab_dataimportexport = [html.H3('TabImportexport')]

tab_dataanalysis = [html.H3('Tabanalytics')]




@app.callback(Output('main-datascreen', 'children'),
              Input('sidebar-tablist', 'value'))
def render_content(tab):
    if tab == 'tab-mainscreen':
        return html.Div(tab_mainscreen_content)
    elif tab == 'tab-config':
        return html.Div(tab_config_content)
    elif tab == 'tab-controldiagram':
        return html.Div(tab_controldiagram)
    elif tab == 'tab-programs':
        return html.Div(tab_programs)
    elif tab == 'tab-dataimportexport':
        return html.Div(tab_dataimportexport)
    elif tab == 'tab-dataanalysis':
        return html.Div(tab_dataanalysis)



sidebar = dbc.Row(sidebar_navigation


)


# End of sidebar-functionality


ui_mainscreen = html.Div(
    [
        dbc.Row(),
        dbc.Row(
            [
                dbc.Col(html.Div(sidebar), width=2),
                dbc.Col(html.Div(id='main-datascreen')),
            ]
        ),
    ]
)




app.layout = dbc.Container(
   [
    dbc.Row(ui_mainscreen),]
)

if __name__ == "__main__":
    app.run_server()
