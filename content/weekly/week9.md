---
title: Week9 - Make something large
#subtitle: Why you'd want to hang out with me
#comments: false
---

This is the week 9 assignment of my fablab-project

For this week my plan was to manufacture the chassis that would hold my final project. In essence, I want it to hold the lamp fixture, growth tanks and the electronical components, and to be transportable. My initial design would be based on a toolbox-type structure.
[![Initial idea](../toolbox.jpg)](../toolbox.jpg)


Essentially it would be a partial rehash of my last years project
https://karlpalo.gitlab.io/fablab2021//assignments/assignment-07/


With the toolboxes "handle" part containing a handle and the LED-growth lamp and the "box" the actual growth tanks.

Here is the FreeCAD-drawing of the project.

[![Initial drawing](../designtoolbog.png)](../designtoolbog.png)

The small holes next to the lamp housing would be for wooden rods. (For carrying the box)

When I got the pieces done, I used the [drawing workbench](https://wiki.freecadweb.org/Drawing_Workbench) to create 2D vectors of my model. These vectors (and the freeCAD-file) can be found below.
[freecad-file](algaetankholder_ver2.FCStd)
[bottom plate](bottomplate.svg)
[side plate](sideplate.svg)
[Fixture/lamp mount](fixture.svg)

Unfortunatly I did not document my workflow using the V-Carve software and the CNC-mill with pictures. For that kind of details, please visit my website from [last year](https://karlpalo.gitlab.io/fablab2021//assignments/assignment-07/)

In V-carve I imported the vectors, positioned them to a block of base material I defined according to the measured plywood dimensions (18mm plywood was in fact 17,7 mm thick) and set the milling speed to a reasonable range.

For milling speed calculations i used a 6mm tool, with a RPM of 14000 and a [chip load of 0.22 ](https://cutter-shop.com/chip-load-chart/) (The chip load was for a 6.35mm blade). This gave a maximum feed rate of 18,480 mm/min = 308 mm/s. This sounded a bit fast for me, so I dropped the feed rate by half to 9200 mm/min, and would use the speed override-function of the milling machine to adjust the speed on the run. (Starting with a too low speed can be remedied by speeding up, but starting with too high speed will just break the tool or detatch the milling piece from the machine base)

The milling was done carefully, but during the manual processing I got a few splinters that could have been avoidided, had I been wearing work gloves. I wore gloves during the CNC-milling, but when I got to hand tools, I forgot the earlier instructions and suffered as a consequence.

The final piece with the added wood-rods, lamp, and the water containers can be seen in the picture below
[![Toolbox](../toolbox_CNC.jpg)](../toolbox_CNC.jpg)
