---
title: About me
subtitle: Karl Mihhels
comments: false
---

# Hello!

[![Picture of me](../me-thumb.jpg)](../me.jpg)
 I am Karl and this is my Fablab project website. I am 30-years old, currently working on my Ph.D. studies here at Aalto University.

<img src="drawing.jpg" alt="drawing" width="200"/>

 My focus area is cellulose chemistry, and my thesis topic is making nanocellulose out of algae.

 As far as fabricating things goes, I attendend Digital fabrication I on 2021, but was not able to complete the other courses that year.
 This is the website for my second try at Fabacademy, hopefully I will do better this time!
